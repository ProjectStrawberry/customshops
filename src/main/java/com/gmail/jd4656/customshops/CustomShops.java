package com.gmail.jd4656.customshops;

import co.aikar.commands.PaperCommandManager;
import com.bekvon.bukkit.residence.Residence;
import com.gmail.jd4656.customshops.types.GroupInfo;
import com.gmail.jd4656.customshops.types.Shop;
import com.gmail.jd4656.customshops.types.ShopAction;
import com.gmail.jd4656.customshops.ui.UIEvents;
import com.griefcraft.lwc.LWCPlugin;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.util.Date;
import java.util.*;
import java.util.concurrent.CompletableFuture;

public class CustomShops extends JavaPlugin {
    public static CustomShops plugin;
    private File dbFile;
    private Connection conn = null;
    private Economy econ = null;
    private BufferedWriter bufferedWriter;

    WorldEditPlugin worldEdit;
    public LWCPlugin lwc;
    public Residence residence;
    FileConfiguration config = null;

    Map<String[], Long> resetTimes = new HashMap<>();
    Set<Material> signMaterials = new HashSet<>(Arrays.asList(Material.OAK_WALL_SIGN, Material.ACACIA_WALL_SIGN, Material.BIRCH_WALL_SIGN, Material.DARK_OAK_WALL_SIGN, Material.JUNGLE_WALL_SIGN, Material.SPRUCE_WALL_SIGN, Material.CRIMSON_WALL_SIGN, Material.WARPED_WALL_SIGN));
    List<String> shopIds = new ArrayList<>();

    Map<Location, Shop> shops = new HashMap<>();
    Map<UUID, ShopAction> pendingActions = new HashMap<>();

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        this.saveDefaultConfig();
        config = plugin.getConfig();
        config.options().copyDefaults(false);
        plugin.saveConfig();

        if (!setupEconomy()) {
            getLogger().severe("CustomShops disabled due to no Vault dependency found.");
            return;
        }

        worldEdit = (WorldEditPlugin) getServer().getPluginManager().getPlugin("WorldEdit");
        lwc = (LWCPlugin) getServer().getPluginManager().getPlugin("LWC");
        residence = (Residence) getServer().getPluginManager().getPlugin("Residence");

        try {
            File logFile = new File(plugin.getDataFolder(), "shop.log");
            if (!logFile.exists()) {
                logFile.getParentFile().mkdirs();
                logFile.createNewFile();
            }
            bufferedWriter = new BufferedWriter(new FileWriter(logFile, true));
        } catch (Exception e) {
            plugin.getLogger().severe("File write error: shop.log");
            return;
        }

        dbFile = new File(plugin.getDataFolder(), "plugin.db");
        if (!dbFile.exists()) {
            try {
                dbFile.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().severe("File write error: plugin.db");
                return;
            }
        }

        try {
            Connection c = plugin.getConnection();
            Statement stmt = c.createStatement();

            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS shops (owner TEXT, type TEXT, item TEXT, material TEXT, groupId TEXT, price REAL, permission TEXT, world TEXT, x INTEGER, y INTEGER, z INTEGER)");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS shopRegions (shopName TEXT, shopId TEXT, owner TEXT, description TEXT, world TEXT, minX INTEGER, minY INTEGER, minZ INTEGER, maxX INTEGER, maxY INTEGER, maxZ INTEGER)");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS users (groupId TEXT, owner TEXT, amount INTEGER, uuid TEXT)");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS groups (groupId TEXT, owner TEXT, amount INTEGER, priceModifier REAL, priceModifierAmount INTEGER, sellLimit INTEGER, resetTime INTEGER, resetsAt INTEGER)");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS messages (uuid TEXT, message TEXT, entryTime INTEGER)");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS uuids (uuid TEXT, name TEXT)");
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS logs (id TEXT, uuid TEXT, owner TEXT, shopType TEXT, shopPrice REAL, action TEXT, unread TEXT, amount INTEGER, money REAL, item TEXT, world TEXT, x INTEGER, y INTEGER, z INTEGER, date INTEGER)");

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("PRAGMA user_version");

            if (rs.getInt("user_version") == 0) {
                stmt.executeUpdate("ALTER TABLE shops ADD COLUMN permission TEXT");
                stmt.executeUpdate("PRAGMA user_version=1");
                plugin.getLogger().info("Upgraded database to version 1");
            }

            rs = stmt.executeQuery("SELECT * FROM groups WHERE resetsAt NOT NULL");

            while (rs.next()) {
                if (rs.getLong("resetTime") == 0) continue;
                String[] id = {rs.getString("groupId"), rs.getString("owner")};
                resetTimes.put(id, rs.getLong("resetsAt"));
            }

            rs = stmt.executeQuery("SELECT * FROM shopRegions");

            while (rs.next()) shopIds.add(rs.getString("shopId"));

            rs = stmt.executeQuery("SELECT * FROM shops");

            while (rs.next()) {
                ItemStack item = Utils.itemFromBase64(rs.getString("item"));
                Location curLocation = new Location(Bukkit.getWorld(rs.getString("world")), rs.getInt("x"), rs.getInt("y"), rs.getInt("z"));
                Shop curShop = new Shop(rs.getString("owner"), rs.getString("type"), rs.getString("groupId"), item, rs.getDouble("price"), curLocation, rs.getString("permission"));

                shops.put(curLocation, curShop);
            }

            stmt.close();
        } catch (Exception e) {
            getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            getLogger().warning("CustomShops failed to load.");
            return;
        }

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        getServer().getPluginManager().registerEvents(new UIEvents(), this);

        PaperCommandManager commandManager = new PaperCommandManager(this);
        commandManager.enableUnstableAPI("help");
        commandManager.registerCommand(new CommandShop(this));

        BukkitTask resetTimer = new BukkitRunnable() {
            @Override
            public void run() {
                Iterator<Map.Entry<String[], Long>> it = resetTimes.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String[], Long> pair = it.next();
                    String[] data = pair.getKey();
                    long resetTime = pair.getValue();
                    if (resetTime == 0) {
                        it.remove();
                        continue;
                    }
                    if (System.currentTimeMillis() < resetTime) continue;

                    try {
                        Connection c = plugin.getConnection();
                        PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM groups WHERE groupId=? AND owner=?");
                        pstmt.setString(1, data[0]);
                        pstmt.setString(2, data[1]);
                        ResultSet rs = pstmt.executeQuery();

                        if (rs.getInt("count(*)") < 1) {
                            it.remove();
                            pstmt.close();
                            continue;
                        }

                        pstmt = c.prepareStatement("SELECT * FROM groups WHERE groupId=? AND owner=?");
                        pstmt.setString(1, data[0]);
                        pstmt.setString(2, data[1]);
                        rs = pstmt.executeQuery();

                        String groupId = rs.getString("groupId");
                        String owner = rs.getString("owner");
                        long resetDelay = rs.getLong("resetTime");

                        pstmt = c.prepareStatement("DELETE FROM users WHERE groupId=? AND owner=?");
                        pstmt.setString(1, groupId);
                        pstmt.setString(2, owner);
                        pstmt.executeUpdate();

                        pstmt = c.prepareStatement("UPDATE groups SET resetsAt=? WHERE groupId=? AND owner=?");
                        pstmt.setLong(1, System.currentTimeMillis() + resetDelay);
                        pstmt.setString(2, data[0]);
                        pstmt.setString(3, data[1]);
                        pstmt.executeUpdate();

                        resetTimes.put(data, System.currentTimeMillis() + resetDelay);

                        pstmt.close();
                    } catch (Exception e) {
                        getLogger().warning("Error in resetTimer:");
                        getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        }.runTaskTimer(this, 1 * 20, 0);

        plugin.getLogger().info("CustomShops loaded.");
    }

    @Override
    public void onDisable() {
        try {
            plugin.getConnection().close();
        } catch (Exception e) {
            getLogger().warning("Error when disabling CustomShops: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }

    public Shop getShop(Location loc) {
        return this.shops.get(loc);
    }

    public Collection<Shop> getShops() {
        return this.shops.values();
    }

    public Connection getConnection() throws Exception {
        if (conn == null ) {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + plugin.dbFile);
        }
        return conn;
    }

    void cacheUUID(UUID uuid, String name) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("INSERT INTO uuids (uuid, name) VALUES (?, ?)");
                    pstmt.setString(1, uuid.toString());
                    pstmt.setString(2, name);

                    pstmt.executeUpdate();
                    pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().info("Error in cacheUUID: ");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    CompletableFuture<Integer> getUnreadLogs(UUID uuid) {
        CompletableFuture<Integer> completableFuture = new CompletableFuture<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT COUNT(*) FROM logs WHERE owner=? AND unread=1");
                    pstmt.setString(1, uuid.toString());

                    ResultSet rs = pstmt.executeQuery();

                    int unread = rs.getInt("COUNT(*)");

                    pstmt.close();

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(unread);
                        }
                    }.runTask(plugin);
                } catch (Exception e) {
                    plugin.getLogger().warning("Error getting unread logs:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);

        return completableFuture;
    }

    CompletableFuture<GroupInfo> getGroupInfo(String groupId, String owner, Player player) {
        CompletableFuture<GroupInfo> completableFuture = new CompletableFuture<>();

        String playerId = player.getUniqueId().toString();

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement pstmt;
                    Connection c = plugin.getConnection();

                    pstmt = c.prepareStatement("SELECT * FROM groups WHERE groupId=? AND owner=?");
                    pstmt.setString(1, groupId);
                    pstmt.setString(2, owner);
                    ResultSet rs = pstmt.executeQuery();

                    GroupInfo info = new GroupInfo();

                    if (rs.next()) {
                        info.setSellAmount(rs.getInt("amount"));
                        info.setLimit(rs.getInt("sellLimit"));
                        info.setPriceModifier(rs.getDouble("priceModifier"));
                        info.setPriceModifierAmount(rs.getInt("priceModifierAmount"));
                        info.setResetsAt(rs.getLong("resetsAt"));
                    }

                    pstmt = c.prepareStatement("SELECT count(*) FROM users WHERE groupId=? AND uuid=? AND owner=?");
                    pstmt.setString(1, groupId);
                    pstmt.setString(2, playerId);
                    pstmt.setString(3, owner);
                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") > 0) {
                        pstmt = c.prepareStatement("SELECT * FROM users WHERE groupId=? AND uuid=? AND owner=?");
                        pstmt.setString(1, groupId);
                        pstmt.setString(2, playerId);
                        pstmt.setString(3, owner);
                        rs = pstmt.executeQuery();
                        info.setCurAmount(rs.getInt("amount"));
                    }

                    pstmt.close();

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(info);
                        }
                    }.runTask(plugin);
                } catch (Exception e) {
                    plugin.getLogger().warning("Error getting groupinfo:");
                    e.printStackTrace();

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(null);
                        }
                    }.runTask(plugin);
                }
            }
        }.runTaskAsynchronously(plugin);

        return completableFuture;
    }

    CompletableFuture<UUID> getUUID(String name) {
        CompletableFuture<UUID> completableFuture = new CompletableFuture<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT uuid FROM uuids WHERE name=?");
                    pstmt.setString(1, name);

                    ResultSet rs = pstmt.executeQuery();

                    if (!rs.next()) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                completableFuture.complete(null);
                            }
                        }.runTask(plugin);
                    } else {
                        UUID uuid = UUID.fromString(rs.getString("uuid"));
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                completableFuture.complete(uuid);
                            }
                        }.runTask(plugin);
                    }

                    pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().info("Error in getUUID: ");
                    e.printStackTrace();
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(null);
                        }
                    }.runTask(plugin);
                }
            }
        }.runTaskAsynchronously(plugin);

        return completableFuture;
    }

    CompletableFuture<Boolean> updateDesc(String id, String desc) {
        CompletableFuture<Boolean> completableFuture = new CompletableFuture<>();
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM shopRegions WHERE shopId=?");
                    pstmt.setString(1, id);
                    ResultSet rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        pstmt.close();

                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                completableFuture.complete(false);
                            }
                        }.runTask(plugin);
                        return;
                    }

                    pstmt = c.prepareStatement("UPDATE shopRegions SET description=? WHERE shopId=?");
                    pstmt.setString(1, desc);
                    pstmt.setString(2, id);
                    pstmt.executeUpdate();
                    pstmt.close();

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(true);
                        }
                    }.runTask(plugin);
                } catch (Exception e) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(false);
                        }
                    }.runTask(plugin);
                    plugin.getLogger().severe("/shop set desc: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);

        return completableFuture;
    }

    void updateShopLimits(String groupId, String owner, int amount, Player player) {
        String uuid = player.getUniqueId().toString();

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT COUNT(*) FROM users WHERE uuid=? AND groupId=? AND owner=?");

                    pstmt.setString(1, uuid);
                    pstmt.setString(2, groupId);
                    pstmt.setString(3, owner);

                    ResultSet rs = pstmt.executeQuery();

                    if (rs.getInt("COUNT(*)") < 1) {
                        pstmt = c.prepareStatement("INSERT INTO users (groupId, owner, amount, uuid) VALUES (?, ?, ?, ?)");
                        pstmt.setString(1, groupId);
                        pstmt.setString(2, owner);
                        pstmt.setInt(3, amount);
                        pstmt.setString(4, player.getUniqueId().toString());
                    } else {
                        pstmt = c.prepareStatement("UPDATE users SET amount = amount + ? WHERE uuid=? AND groupId=? AND owner=?");
                        pstmt.setInt(1, amount);
                        pstmt.setString(2, player.getUniqueId().toString());
                        pstmt.setString(3, groupId);
                        pstmt.setString(4, owner);
                    }
                    pstmt.executeUpdate();
                    pstmt.close();
                } catch (Exception e) {
                    plugin.getLogger().warning("Error updating shop limits: ");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(plugin);
    }

    CompletableFuture<Location> getClosestShopAsync(Location location, Material material) {
        CompletableFuture<Location> completableFuture = new CompletableFuture<>();

        final int x = location.getBlockX();
        final int z = location.getBlockZ();
        final String worldName = location.getWorld().getName();

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement pstmt;
                    ResultSet rs;
                    Connection c = plugin.getConnection();

                    pstmt = c.prepareStatement("SELECT count(*) FROM shops WHERE material=? AND world=?");
                    pstmt.setString(1, material.toString());
                    pstmt.setString(2, worldName);
                    rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 1) {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                completableFuture.complete(null);
                            }
                        }.runTask(plugin);
                        pstmt.close();
                        return;
                    }

                    pstmt = c.prepareStatement("SELECT * FROM shops WHERE material=? AND world=? ORDER BY ((x-?)*(x-?)) + ((z - ?)*(z - ?)) ASC LIMIT 5");
                    pstmt.setString(1, material.toString());
                    pstmt.setString(2, worldName);
                    pstmt.setInt(3, x);
                    pstmt.setInt(4, x);
                    pstmt.setInt(5, z);
                    pstmt.setInt(6, z);

                    rs = pstmt.executeQuery();

                    final int finalX = rs.getInt("x");
                    final int finalY = rs.getInt("y");
                    final int finalZ = rs.getInt("z");

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(new Location(location.getWorld(), finalX, finalY, finalZ));
                        }
                    }.runTask(plugin);
                    pstmt.close();
                } catch (Exception e) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(null);
                        }
                    }.runTask(plugin);
                    plugin.getLogger().severe("/shop closest: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(this);

        return completableFuture;
    }

    /*
     * from https://github.com/KaiKikuchi/QuickShop/blob/master/src/main/java/org/maxgamer/quickshop/Command/QS.java#L614
     */
    Location lookAt(Location loc, Location lookat) {
        // Clone the loc to prevent applied changes to the input loc
        loc = loc.clone();
        // Values of change in distance (make it relative)
        double dx = lookat.getX() - loc.getX();
        double dy = lookat.getY() - loc.getY();
        double dz = lookat.getZ() - loc.getZ();
        // Set yaw
        if (dx != 0) {
            // Set yaw start value based on dx
            if (dx < 0) {
                loc.setYaw((float) (1.5 * Math.PI));
            } else {
                loc.setYaw((float) (0.5 * Math.PI));
            }
            loc.setYaw(loc.getYaw() - (float) Math.atan(dz / dx));
        } else if (dz < 0) {
            loc.setYaw((float) Math.PI);
        }
        // Get the distance from dx/dz
        double dxz = Math.sqrt(Math.pow(dx, 2) + Math.pow(dz, 2));
        float pitch = (float) -Math.atan(dy / dxz);
        // Set values, convert to degrees
        // Minecraft yaw (vertical) angles are inverted (negative)
        loc.setYaw(-loc.getYaw() * 180f / (float) Math.PI + 360);
        // But pitch angles are normal
        loc.setPitch(pitch * 180f / (float) Math.PI);
        return loc;
    }

    ItemStack fixItem(ItemStack item) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            ItemStack inputItem = item.clone();

            dataOutput.writeObject(inputItem);
            dataOutput.close();

            String base64 = Base64Coder.encodeLines(outputStream.toByteArray());

            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(base64));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack newItem = (ItemStack) dataInput.readObject();
            dataInput.close();

            return newItem;
        } catch (IOException | ClassNotFoundException ignored) {}

        return item;
    }

    String convertTime(long ms) {
        if (ms < 1000) {
            return "less than one second";
        }

        long seconds = (ms / 1000) % 60;
        long minutes = (ms / (1000 * 60)) % 60;
        long hours = (ms / (1000 * 60 * 60)) % 24;
        long days = (ms / (1000 * 60 * 60 * 24));

        String reply = "";
        if (days > 0) {
            reply += days + (days == 1 ? " day " : " days ");
        }
        if (hours > 0) {
            reply += hours + (hours == 1 ? " hour " : " hours ");
        }
        if (minutes > 0) {
            reply += minutes + (minutes == 1 ? " minute " : " minutes ");
        }
        if (seconds > 0 && hours < 1) {
            reply += seconds + (seconds == 1 ? " second " : " seconds ");
        }

        return reply.trim();
    }

    BigDecimal truncateDecimal(double x) {
        if (x > 0) {
            return new BigDecimal(String.valueOf(x)).setScale(3, BigDecimal.ROUND_FLOOR);
        } else {
            return new BigDecimal(String.valueOf(x)).setScale(3, BigDecimal.ROUND_CEILING);
        }
    }

    int stringToTime(String time) {
        String validModifiers = "smhd";
        char timeModifier = time.charAt(time.length() - 1);
        int timeNumber;
        int timeDuration = 0;
        if (validModifiers.indexOf(timeModifier) < 0) {
            return 0;
        }

        try {
            timeNumber = Integer.parseInt(time.substring(0, time.length() - 1));
        } catch (NumberFormatException e) {
            return 0;
        }

        if (timeModifier == 's') timeDuration = timeNumber * 1000;
        if (timeModifier == 'm') timeDuration = timeNumber * 60 * 1000;
        if (timeModifier == 'h') timeDuration = timeNumber * 60 * 60 * 1000;
        if (timeModifier == 'd') timeDuration = timeNumber * 24 * 60 * 60 * 1000;

        return timeDuration;
    }

    List<BaseComponent> pageify(List messages, int pageNumber, String pageCmd) {
        int pageSize = 8;
        List<BaseComponent> page = new ArrayList<>();

        int totalPages = messages.size() / pageSize + ((messages.size() % pageSize == 0) ? 0 : 1);
        if (totalPages < pageNumber) pageNumber = totalPages;

        int count = 0;
        int curPageNum;

        page.add(new TextComponent(ChatColor.DARK_GREEN + "Page " + ChatColor.GOLD + pageNumber + ChatColor.DARK_GREEN + " of " + ChatColor.GOLD + totalPages));

        for (Object curPage : messages) {
            count++;
            curPageNum = count / pageSize + ((count % pageSize == 0) ? 0 : 1);
            if (curPageNum != pageNumber) continue;
            if (!(curPage instanceof BaseComponent)) curPage = new TextComponent((String) curPage);
            page.add((BaseComponent) curPage);
        }

        TextComponent footer = new TextComponent();
        TextComponent backButton = new TextComponent(ChatColor.DARK_GREEN + "[Last Page]");
        if (pageNumber > 1) backButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, pageCmd + " " + (pageNumber - 1)));
        TextComponent nextButton = new TextComponent(ChatColor.DARK_GREEN + "[Next Page]");
        if (pageNumber < totalPages) nextButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, pageCmd + " " + (pageNumber + 1)));

        footer.addExtra(backButton);
        footer.addExtra(" | ");
        footer.addExtra(nextButton);

        page.add(footer);

        return page;
    }

    void log(String message) {
        Date date = Calendar.getInstance().getTime();
        Timestamp time = new Timestamp(date.getTime());
        try {
            bufferedWriter.write("[" + time.toString() + "] " + message);
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (Exception e) {
            plugin.getLogger().warning("Error writing log file.");
            e.printStackTrace();
        }
    }

    private TextComponent getItemText(net.minecraft.server.v1_16_R3.ItemStack item) {
        return new TextComponent(item.save(new net.minecraft.server.v1_16_R3.NBTTagCompound()).toString());
    }

    HoverEvent getItemHover(net.minecraft.server.v1_16_R3.ItemStack item) {
        return new HoverEvent(HoverEvent.Action.SHOW_ITEM, new BaseComponent[] { getItemText(item) });
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    Economy getEconomy() {
        return econ;
    }
}
