package com.gmail.jd4656.customshops;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import com.gmail.jd4656.customshops.types.Shop;
import com.gmail.jd4656.customshops.types.ShopAction;
import com.gmail.jd4656.customshops.types.ShopActions;
import com.gmail.jd4656.customshops.ui.MainPage;
import com.gmail.jd4656.customshops.ui.RecentTransactions;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.BukkitPlayer;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@CommandAlias("shop")
public class CommandShop extends BaseCommand {

    private CustomShops plugin;

    CommandShop(CustomShops p) {
        plugin = p;
    }

    @Default
    @CommandPermission("customshops.use")
    public void onCommand(Player player, String args[]) {
        new MainPage(player);
    }

    @Subcommand("fix")
    @Description("Fixes a shop chest with items from before 1.16")
    public void fixCommand(Player player) {
        ShopAction action = new ShopAction();
        action.setPlayerLocation(player.getLocation());
        action.setAction(ShopActions.FIX);

        plugin.pendingActions.put(player.getUniqueId(), action);
        player.sendMessage(ChatColor.GREEN + "Click on the shop you would like to fix.");
    }

    @Subcommand("reload")
    @Description("Reloads the configuration files")
    @CommandPermission("customshops.reload")
    public void reloadCommand(CommandSender sender) {
        plugin.reloadConfig();
        plugin.config = plugin.getConfig();

        sender.sendMessage(ChatColor.DARK_GREEN + "Configuration reloaded.");
    }

    @Subcommand("cancel")
    @Description("Cancels any pending actions")
    public void cancelCommand(Player player) {
        plugin.pendingActions.remove(player.getUniqueId());
        player.sendMessage(ChatColor.DARK_GREEN + "Any pending actions have been cancelled.");
    }

    @Subcommand("closest")
    @Description("Finds the closest shop selling the specified item")
    public void closestCommand(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop closest <item>");
            return;
        }

        Material material = Material.matchMaterial(String.join(" ", args));
        if (material == null) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Item " + ChatColor.RED + args[0] + ChatColor.DARK_RED + " not found.");
            return;
        }

        plugin.getClosestShopAsync(player.getLocation(), material).thenAccept(location -> {
            if (location == null) {
                player.sendMessage(ChatColor.RED + "Could not find any shops containing " + Utils.materialName(material));
                return;
            }

            int distance = (int) Math.floor(Math.sqrt(player.getLocation().distanceSquared(location)));

            player.sendMessage(ChatColor.GREEN + "Shop is " + distance + (distance == 1 ? " block" : " blocks") + " away from you.");
            player.teleport(plugin.lookAt(player.getEyeLocation().clone(), location.add(0.5, 0.5, 0.5)).add(0, -1.62, 0), PlayerTeleportEvent.TeleportCause.UNKNOWN);
        });
    }

    @Subcommand("reset")
    @CommandPermission("customshops.create")
    public void resetCommand(Player player) {
        ShopAction action = new ShopAction();
        action.setPlayerLocation(player.getLocation());
        action.setAction(ShopActions.RESET_MODIFIERS);

        plugin.pendingActions.put(player.getUniqueId(), action);

        player.sendMessage(ChatColor.GOLD + "Punch the shop you would like to reset the price modifier and limit on.");
    }

    @Subcommand("history")
    public void historyCommand(Player player) {
        new RecentTransactions(player, 1);
    }

    @Subcommand("region")
    @CommandPermission("customshops.admin")
    public class RegionCommands extends BaseCommand {
        @Subcommand("create")
        @Description("Creates a shop region")
        public void createCommand(Player player, String[] args) {
            if (args.length < 3) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop region create <shopname> <owner> <description>");
                return;
            }

            String shopName = args[0];
            String shopId = shopName.replaceAll("[^aA0-zZ9]", "").toLowerCase();
            String desc = "";
            for (int i = 2; i < args.length; i++) desc += args[i] + " ";
            desc = desc.trim();

            final String finalDesc = desc;

            plugin.getUUID(args[1]).thenAccept(uuid -> {
                if (uuid == null) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Player not found.");
                    return;
                }

               OfflinePlayer owner = Bukkit.getOfflinePlayer(uuid);

                BukkitPlayer bPlayer = BukkitAdapter.adapt(player);
                Region region = null;
                try {
                    region = WorldEdit.getInstance().getSessionManager().get(bPlayer).getSelection(bPlayer.getWorld());
                } catch (IncompleteRegionException ignored) {}

                if (region == null) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Please select a region with WorldEdit first.");
                    return;
                }

                BlockVector3 minVec = region.getMinimumPoint();
                BlockVector3 maxVec = region.getMaximumPoint();

                try {
                    Connection c = plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM shopRegions WHERE shopId=?");
                    pstmt.setString(1, shopId);
                    ResultSet rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") > 0) {
                        player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A shop region with that name already exists.");
                        pstmt.close();
                        return;
                    }

                    pstmt = c.prepareStatement("INSERT INTO shopRegions (shopName, shopId, owner, description, minX, minY, minZ, maxX, maxY, maxZ, world) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    pstmt.setString(1, shopName);
                    pstmt.setString(2, shopId);
                    pstmt.setString(3, owner.getUniqueId().toString());
                    pstmt.setString(4, finalDesc);
                    pstmt.setInt(5, minVec.getBlockX());
                    pstmt.setInt(6, minVec.getBlockY());
                    pstmt.setInt(7, minVec.getBlockZ());
                    pstmt.setInt(8, maxVec.getBlockX());
                    pstmt.setInt(9, maxVec.getBlockY());
                    pstmt.setInt(10, maxVec.getBlockZ());
                    pstmt.setString(11, player.getWorld().getName());
                    pstmt.executeUpdate();
                    pstmt.close();

                    plugin.shopIds.add(shopId);

                    player.sendMessage(ChatColor.DARK_GREEN + "That shop region has been created.");
                    return;
                } catch (Exception e) {
                    player.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                    plugin.getLogger().severe("/shop create: " + e.getClass().getName() + ": " + e.getMessage());
                    e.printStackTrace();
                    return;
                }
            });
        }

        @Subcommand("remove")
        @Description("Removes a shop region")
        public void removeCommand(CommandSender sender, String[] args) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop region remove <shopid>");
                return;
            }

            String shopName = args[0];
            String shopId = shopName.replaceAll("[^aA0-zZ9]", "").toLowerCase();

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM shopRegions WHERE shopId=?");
                pstmt.setString(1, shopId);
                ResultSet rs = pstmt.executeQuery();

                if (rs.getInt("count(*)") < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A shop region with that name does not exist.");
                    pstmt.close();
                    return;
                }

                pstmt = c.prepareStatement("DELETE FROM shopRegions WHERE shopId=?");
                pstmt.setString(1, shopId);
                pstmt.executeUpdate();
                pstmt.close();

                plugin.shopIds.remove(shopId);

                sender.sendMessage(ChatColor.DARK_GREEN + "That shop region has been removed.");
                return;
            } catch (Exception e) {
                sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/shop remove: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return;
            }
        }
    }

    @Subcommand("set")
    @CommandPermission("customshops.create")
    public class SetCommands extends BaseCommand {
        @Subcommand("permission")
        @Description("Sets the permission needed to use this shop.")
        @CommandPermission("customshops.admin")
        public void permissionCommand(Player player, String[] args) {
            if (args.length < 1) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop set permission <permission>");
                return;
            }

            ShopAction action = new ShopAction();
            action.setPlayerLocation(player.getLocation());
            action.setAction(ShopActions.SET_PERMISSION);

            if (args[0].equals("off")) {
                action.setActionArgs("");
            } else {
                action.setActionArgs(args[0]);
            }

            plugin.pendingActions.put(player.getUniqueId(), action);
            player.sendMessage(ChatColor.GOLD + "Punch the chest you want to set the permission on.");
        }

        @Subcommand("type")
        @Description("Sets whether a shop buys or sells items")
        public void typeCommand(Player player, String[] args) {
            if (args.length < 1) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop set type <buy/sell>");
                return;
            }

            String type = args[0];
            if (!type.equals("buy") && !type.equals("sell")) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid type.");
                return;
            }

            ShopAction action = new ShopAction();
            action.setPlayerLocation(player.getLocation());
            action.setAction(ShopActions.CHANGE_TYPE);
            action.setActionArgs(type);

            plugin.pendingActions.put(player.getUniqueId(), action);

            player.sendMessage(ChatColor.GOLD + "Punch the chest you want to change the type on.");
        }

        @Subcommand("owner")
        @Description("Sets the owner of a shop")
        @CommandPermission("customshops.admin")
        public void ownerCommand(Player player, String[] args) {
            if (args.length < 1) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop set owner <owner>");
                return;
            }

            String owner = args[0];
            Player targetPlayer = null;
            if (!owner.equals("admin")) targetPlayer = Bukkit.getPlayer(owner);

            if (!owner.equals("admin") && targetPlayer == null) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player is not online.");
                return;
            }

            if (targetPlayer != null) owner = targetPlayer.getUniqueId().toString();

            ShopAction action = new ShopAction();
            action.setPlayerLocation(player.getLocation());
            action.setAction(ShopActions.CHANGE_OWNER);
            action.setActionArgs(owner);

            plugin.pendingActions.put(player.getUniqueId(), action);

            player.sendMessage(ChatColor.GOLD + "Punch the chest you would like to change the owner on.");
        }

        @Subcommand("price")
        @Description("Sets the price of a shop")
        public void priceCommand(Player player, String[] args) {
            if (args.length < 1) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop set price <amount>");
                return;
            }

            ShopAction action = new ShopAction();
            action.setPlayerLocation(player.getLocation());
            action.setAction(ShopActions.CHANGE_PRICE);
            action.setActionArgs(args[0]);

            plugin.pendingActions.put(player.getUniqueId(), action);

            player.sendMessage(ChatColor.GOLD + "Punch the chest you would like to change the price on.");
        }

        @Subcommand("find")
        @Description("Searches for an item in shops")
        public void findCommand(Player player, String[] args) {
            if (args.length < 2) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: /shop find <item> - " + ChatColor.GOLD + "Shows all the premium shops for a specific item.");
                return;
            }

            String searchString = "";
            for (int i=1; i<args.length; i++) searchString += args[i] + " ";

            searchString = searchString.trim();

            Material material = Material.matchMaterial(searchString);
            if (material == null) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Item " + ChatColor.RED + args[0] + ChatColor.DARK_RED + " not found.");
                return;
            }

            int page = 1;
            if (args.length > 2) {
                try {
                    page = Integer.parseInt(args[2]);
                } catch (NumberFormatException ignored) {}
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT * FROM shops WHERE material=? AND world=?");
                pstmt.setString(1, material.toString());
                pstmt.setString(2, player.getWorld().getName());
                ResultSet rs = pstmt.executeQuery();

                PreparedStatement regionstmt = null;
                ResultSet regionResults;

                List<BaseComponent> results = new LinkedList<>();

                while (rs.next()) {
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(rs.getString("item")));
                    BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
                    ItemStack item = (ItemStack) dataInput.readObject();
                    dataInput.close();

                    regionstmt = c.prepareStatement("SELECT * FROM shopRegions WHERE world=? AND (? BETWEEN minX AND maxX) AND (? BETWEEN minY AND maxY) AND (? BETWEEN minZ AND maxZ)");
                    regionstmt.setString(1, player.getWorld().getName());
                    regionstmt.setInt(2, rs.getInt("x"));
                    regionstmt.setInt(3, rs.getInt("y"));
                    regionstmt.setInt(4, rs.getInt("z"));
                    regionResults = regionstmt.executeQuery();

                    while (regionResults.next()) {
                        OfflinePlayer curPlayer = Bukkit.getOfflinePlayer(UUID.fromString(regionResults.getString("owner")));
                        BaseComponent curResult = new TextComponent();

                        TextComponent curItem = new TextComponent(Utils.materialName(material));
                        curItem.setColor(ChatColor.GOLD.asBungee());
                        curItem.setHoverEvent(plugin.getItemHover(CraftItemStack.asNMSCopy(item)));

                        TextComponent part2 = new TextComponent((rs.getString("type").equals("buy") ? " sold" : " purchased") + " at ");
                        part2.setColor(ChatColor.DARK_GREEN.asBungee());

                        TextComponent playerName = new TextComponent(curPlayer.getName() + "'s");
                        playerName.setColor(ChatColor.GOLD.asBungee());

                        TextComponent part3 = new TextComponent(" shop, " + regionResults.getString("description"));
                        part3.setColor(ChatColor.DARK_GREEN.asBungee());

                        curResult.addExtra(curItem);
                        curResult.addExtra(part2);
                        curResult.addExtra(playerName);
                        curResult.addExtra(part3);

                        results.add(curResult);
                    }
                }

                if (regionstmt != null) regionstmt.close();
                pstmt.close();

                if (results.size() > 0) {
                    List<BaseComponent> messages = plugin.pageify(results, page, "/shop find");
                    for (BaseComponent message : messages) player.spigot().sendMessage(message);
                } else {
                    player.sendMessage(ChatColor.RED + "No shops found that sell that item.");
                }
            } catch (Exception e) {
                player.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/shop find: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
            }
        }

        @Subcommand("limit")
        @Description("Sets the buy/sell limit on a shop")
        public void limitCommand(Player player, String[] args) {
            if (args.length < 1) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop set limit <amount>");
                return;
            }

            try {
                Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid limit.");
                return;
            }

            ShopAction action = new ShopAction();
            action.setPlayerLocation(player.getLocation());
            action.setAction(ShopActions.SET_LIMIT);
            action.setActionArgs(args[0]);

            plugin.pendingActions.put(player.getUniqueId(), action);

            player.sendMessage(ChatColor.GOLD + "Punch the chest you would like to set the limit on.");
        }

        @Subcommand("modifier")
        @Description("Sets the price modifier on a shop")
        public void modifierCommand(Player player, String[] args) {
            if (args.length < 2) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop set modifier <percentage> <amount>");
                return;
            }

            double percentage = 0;
            int amount = 0;

            try {
                percentage = Double.parseDouble(args[0]);
            } catch (NumberFormatException e) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid percentage");
                return;
            }

            try {
                amount = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid amount.");
                return;
            }

            ShopAction action = new ShopAction();
            action.setPlayerLocation(player.getLocation());
            action.setAction(ShopActions.SET_MODIFIER);
            action.setActionArgs(args);

            plugin.pendingActions.put(player.getUniqueId(), action);

            player.sendMessage(ChatColor.GOLD + "Punch the chest you would like to set the price modifier on.");
        }

        @Subcommand("time")
        @Description("Sets how often the shop should reset if using a price modifier or limit")
        public void timeCommand(Player player, String[] args) {
            if (args.length < 1) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop set time <time (1s/m/h/d)>");
                return;
            }

            int timeInt = plugin.stringToTime(args[0]);
            if (timeInt == 0) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid time.");
                return;
            }

            ShopAction action = new ShopAction();
            action.setPlayerLocation(player.getLocation());
            action.setAction(ShopActions.RESET_TIME);
            action.setActionArgs(args[0]);

            plugin.pendingActions.put(player.getUniqueId(), action);

            player.sendMessage(ChatColor.GOLD + "Punch the chest you would like to set the reset timer on");
        }

        @Subcommand("group")
        @Description("Lets you group shops together to share a price modifier or limit.")
        public void groupCommand(Player player, String[] args) {
            if (args.length < 1) {
                player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop set group <group>");
                return;
            }

            ShopAction action = new ShopAction();
            action.setPlayerLocation(player.getLocation());
            action.setAction(ShopActions.SET_GROUP);
            action.setActionArgs(args[0]);

            plugin.pendingActions.put(player.getUniqueId(), action);

            player.sendMessage(ChatColor.GOLD + "Punch the chest you would like to set the group on.");
        }

        @Subcommand("desc")
        @Description("Lets you set a description for a shop.")
        @CommandPermission("customshops.admin")
        public void descCommand(CommandSender sender, String[] args) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /shop set desc <shopid> <desciption>");
                return;
            }

            String shopName = args[0];
            String shopId = shopName.replaceAll("[^aA0-zZ9]", "").toLowerCase();
            String desc = "";
            for (int i = 1; i < args.length; i++) desc += args[i] + " ";
            desc = desc.trim();

            plugin.updateDesc(shopId, desc).thenAccept(success -> {
                if (!success) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "A shop region with that name does not exist.");
                } else {
                    sender.sendMessage(ChatColor.DARK_GREEN + "That shop description has been updated.");
                }
            });
        }
    }

    @Subcommand("amount")
    @Description("Used internally when making a transaction with a shop")
    public void amountCommand(Player player, String[] args) {
        if (args.length < 1) {
            player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop amount <amount>");
            return;
        }

        ShopAction action = plugin.pendingActions.get(player.getUniqueId());

        if (action == null || action.getAction() != ShopActions.TRANSACTION) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Please punch a shop before using this command.");
            return;
        }

        int amount;
        try {
            amount = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid number.");
            plugin.pendingActions.remove(player.getUniqueId());
            return;
        }

        if (amount < 1) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid amount.");
            plugin.pendingActions.remove(player.getUniqueId());
            return;
        }

        Material targetMaterial = action.getShopLocation().getBlock().getType();
        Location shopLocation = action.getShopLocation();

        if (!plugin.shops.containsKey(shopLocation)) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That shop no longer exists.");
            plugin.pendingActions.remove(player.getUniqueId());
            return;
        }

        Shop shop = plugin.shops.get(shopLocation);

        plugin.getGroupInfo(shop.getGroupId(), shop.getOwner(), player).thenAccept(info -> {
            String type = shop.getType();
            String owner = shop.getOwner();
            String groupId = shop.getGroupId();
            int sellAmount = info.getSellAmount();
            int limit = info.getLimit();
            int curAmount = info.getCurAmount();
            int priceModifierAmount = info.getPriceModifierAmount();
            double originalPrice = shop.getPrice();
            double price = shop.getPrice();
            double priceModifier = info.getPriceModifier();
            long resetsAt = info.getResetsAt();

            ItemStack item = shop.getItem();
            OfflinePlayer ownerPlayer = null;

            if (!owner.equals("admin")) ownerPlayer = Bukkit.getOfflinePlayer(UUID.fromString(owner));

            if (sellAmount != 0) {
                if ((amount % sellAmount) != 0) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This shop only " + type + "s in multiples of " + sellAmount + ".");
                    plugin.pendingActions.remove(player.getUniqueId());
                    return;
                }
            }

            if (limit != 0) {
                if (limit < curAmount) {
                    player.sendMessage(ChatColor.RED + "You've hit the " + (type.equals("buy") ? "purchase" : "sell") + " limit of this shop.");
                    if (resetsAt > 0) {
                        player.sendMessage(ChatColor.RED + "You may not " + (type.equals("buy") ? "buy from" : "sell to") + " this shop for another: " + plugin.convertTime(resetsAt - System.currentTimeMillis()));
                    } else {
                        player.sendMessage(ChatColor.RED + "You may not " + (type.equals("buy") ? "buy from" : "sell to") + " this shop until it is reset.");
                    }
                    plugin.pendingActions.remove(player.getUniqueId());
                    return;
                } else if (limit < (curAmount + amount)) {
                    player.sendMessage(ChatColor.RED + (type.equals("buy") ? "Selling" : "Buying") + " that much would put you over the limit of this shop.");
                    player.sendMessage(ChatColor.RED + "You may only " + (type.equals("buy") ? "buy" : "sell") + " " + (limit - curAmount) + ((limit - curAmount) == 1 ? " more item." : " more items."));
                    plugin.pendingActions.remove(player.getUniqueId());
                    return;
                }
            }

            if (priceModifier > 0 && amount > (priceModifierAmount - (curAmount % priceModifierAmount))) {
                int remaining = (priceModifierAmount - (curAmount % priceModifierAmount));
                player.sendMessage(ChatColor.RED + "You may only " + (type.equals("buy") ? "sell" : "buy") + " " + remaining + (remaining == 1 ? " more item " : " more items ") + "until the price " + (!type.equals("buy") ? "increases." : "decreases."));
                plugin.pendingActions.remove(player.getUniqueId());
                return;
            }

            if (priceModifier > 0 && curAmount > priceModifierAmount) {
                double timesToDecrease = Math.floor((double) curAmount / priceModifierAmount);
                for (int i = 0; i < timesToDecrease; i++) {
                    if (type.equals("sell")) price = (price + (priceModifier / 100) * price);
                    if (type.equals("buy")) price = (price - (priceModifier / 100) * price);
                }
            } else if (priceModifier > 0 && curAmount == priceModifierAmount) {
                if (type.equals("sell")) price = (price + (priceModifier / 100) * price);
                if (type.equals("buy")) price = (price - (priceModifier / 100) * price);
            }

            price = plugin.truncateDecimal(price).doubleValue();

            Economy econ = plugin.getEconomy();
            double playerBalance = econ.getBalance(player);
            double cost = price * amount;

            if ((cost - playerBalance) > 0 && type.equals("sell")) {
                player.sendMessage(ChatColor.RED + "You need $" + (cost - playerBalance) + " more to buy that much.");
                plugin.pendingActions.remove(player.getUniqueId());
                return;
            }

            if (type.equals("buy") && ownerPlayer != null) {
                double ownerBalance = econ.getBalance(ownerPlayer);
                if (ownerBalance < cost) {
                    player.sendMessage(ChatColor.RED + "The shop owner does not have enough money to buy that much.");
                    plugin.pendingActions.remove(player.getUniqueId());
                    return;
                }
            }

            Block block = shopLocation.getBlock();
            if (!(block.getState() instanceof Container)) {
                // I don't think this is possible, but just in case
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That shop is no longer valid.");
                return;
            }

            Inventory inv = ((Container) shopLocation.getBlock().getState()).getInventory();

            if (type.equals("buy")) {
                if (Utils.countItems(player.getInventory(), item) < amount) {
                    player.sendMessage(ChatColor.RED + "You do not have that many items to sell.");
                    plugin.pendingActions.remove(player.getUniqueId());
                    return;
                }
                if (ownerPlayer != null) {
                    int freeSlots = Utils.freeSpace(inv, item);
                    if (freeSlots < amount) {
                        player.sendMessage(ChatColor.RED + "That shop does not have enough room to buy that many items. It only has room for " + freeSlots + (freeSlots == 1 ? " more item." : " more items."));
                        plugin.pendingActions.remove(player.getUniqueId());
                        return;
                    }

                    List<ItemStack> removedItems = Utils.removeItems(player.getInventory(), item, amount);
                    for (ItemStack thisItem : removedItems) {
                        inv.addItem(thisItem);
                    }

                    econ.withdrawPlayer(ownerPlayer, cost);
                    shop.updateSign();
                } else {
                    Utils.removeItems(player.getInventory(), item, amount);
                }

                double taxAmt = plugin.config.getDouble("tax");
                double tax = (taxAmt / 100) * cost;

                econ.depositPlayer(player, cost - tax);

                if (priceModifier > 0 || limit > 0) {
                    plugin.updateShopLimits(groupId, owner, amount, player);
                }

                plugin.pendingActions.remove(player.getUniqueId());
                player.sendMessage(ChatColor.GREEN + "You've sold " + ChatColor.YELLOW + amount + (amount == 1 ? " item" : " items") + ChatColor.GREEN + " for " + ChatColor.YELLOW + "$" + String.format("%.2f", cost) + ChatColor.GREEN + (tax > 0 ? ", and you paid $" + String.format("%.2f", tax) + " in taxes." : ""));

                shop.logTransaction(player, "SELL", amount, cost);
            } else {
                int freeSlots = Utils.freeSpace(player.getInventory(), item);
                if (freeSlots < amount) {
                    player.sendMessage(ChatColor.RED + "You only have room for " + ChatColor.YELLOW + freeSlots + ChatColor.RED + (freeSlots == 1 ? " more item." : " more items."));
                    plugin.pendingActions.remove(player.getUniqueId());
                    return;
                }

                if (ownerPlayer != null) {
                    if (Utils.countItems(inv, item) < amount) {
                        player.sendMessage(ChatColor.RED + "That shop does not have that many items to sell.");
                        plugin.pendingActions.remove(player.getUniqueId());
                        return;
                    }

                    List<ItemStack> removedItems = Utils.removeItems(inv, item, amount);
                    for (ItemStack thisItem : removedItems) {
                        player.getInventory().addItem(thisItem);
                    }

                    double taxAmt = plugin.config.getDouble("tax");
                    double tax = (taxAmt / 100) * cost;

                    econ.depositPlayer(ownerPlayer, cost - tax);
                    shop.updateSign();
                } else {
                    int addedItems = 0;
                    while (addedItems < amount) {
                        ItemStack curItem = item.clone();
                        if ((curItem.getMaxStackSize() + addedItems) < amount) {
                            curItem.setAmount(curItem.getMaxStackSize());
                            addedItems += curItem.getMaxStackSize();
                        } else {
                            int amountToAdd = (amount - addedItems);
                            curItem.setAmount(amountToAdd);
                            addedItems += amountToAdd;
                        }

                        player.getInventory().addItem(curItem);
                    }
                }

                econ.withdrawPlayer(player, cost);

                if (priceModifier > 0 || limit > 0) {
                    plugin.updateShopLimits(groupId, owner, amount, player);
                }

                plugin.pendingActions.remove(player.getUniqueId());
                player.sendMessage(ChatColor.GREEN + "You've purchased " + ChatColor.YELLOW + amount + (amount == 1 ? " item" : " items") + ChatColor.GREEN + " for " + ChatColor.YELLOW + "$" + String.format("%.2f", cost));
                shop.logTransaction(player, "BUY", amount, cost);
            }
        });
    }

    @Subcommand("shopprice")
    @Description("Used internally when creating a shop")
    @CommandPermission("customshops.create")
    public void shoppriceCommand(Player player, String[] args) {
        ShopAction action = plugin.pendingActions.get(player.getUniqueId());
        if (action == null || action.getAction() != ShopActions.CREATE) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You must punch a chest with an item first.");
            return;
        }
        if (args.length < 1) {
            player.sendMessage(ChatColor.DARK_GREEN + "Usage: " + ChatColor.GREEN + "/shop shopprice <price>");
            return;
        }

        double price;
        try {
            price = Double.parseDouble(args[0]);
        } catch (NumberFormatException e) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid price.");
            plugin.pendingActions.remove(player.getUniqueId());
            return;
        }

        ItemStack item = action.getItem();
        Material targetMaterial = action.getShopLocation().getBlock().getType();
        Location shopLocation = action.getShopLocation();

        if (!Utils.validShopMaterial(targetMaterial)) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That chest no longer exists.");
            return;
        }

        if (plugin.shops.containsKey(shopLocation)) {
            player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That chest is already a shop.");
            return;
        }

        Shop shop = new Shop(player.getUniqueId().toString(), "sell", item.getType().toString(), item, price, shopLocation, null);
        plugin.shops.put(shopLocation, shop);
        shop.save();

        player.sendMessage(ChatColor.GREEN + "You've created a shop");
    }

    @HelpCommand
    public void doHelp(CommandSender sender, CommandHelp help) {
        help.showHelp();
    }
}
