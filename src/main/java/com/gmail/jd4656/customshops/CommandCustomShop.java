package com.gmail.jd4656.customshops;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.StringUtil;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.sql.*;
import java.util.*;

public class CommandCustomShop implements TabExecutor {
    private CustomShops plugin;

    CommandCustomShop(CustomShops p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }
        Player player = (Player) sender;

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GREEN + "CustomShop commands:");
            sender.sendMessage(ChatColor.DARK_GREEN + "/shop cancel - " + ChatColor.GOLD + "Cancels any pending actions. (Shop creation / edit / purchase)");
            sender.sendMessage(ChatColor.DARK_GREEN + "/shop reset - " + ChatColor.GOLD + "Resets the price modifier and buy/sell limit.");
            sender.sendMessage(ChatColor.DARK_GREEN + "/shop find <item> - " + ChatColor.GOLD + "Lists all shops buying or selling an item.");
            sender.sendMessage(ChatColor.DARK_GREEN + "/shop search <item> - " + ChatColor.GOLD + "Finds the nearest shop selling / buying <item>.");
            if (sender.hasPermission("customshops.admin")) {
                sender.sendMessage(ChatColor.DARK_GREEN + "/shop create <shopname> <owner> <description> - " + ChatColor.GOLD + "Creates a shop region.");
                sender.sendMessage(ChatColor.DARK_GREEN + "/shop remove <shopid> - " + ChatColor.GOLD + "Removes a shop region.");
                sender.sendMessage(ChatColor.DARK_GREEN + "/shop reload - " + ChatColor.GOLD + "Reloads the configuration file.");
            }
            setHelp(sender);
            return true;
        }

        if (args[0].equals("import")) {
            if (!sender.hasPermission("customshops.reload")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            BukkitTask task = new BukkitRunnable() {
                @Override
                public void run() {
                    File file = new File("plugins/QuickShop/shops.db");
                    if (!file.exists()) {
                        sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "Could not read QuickShops database file.");
                    }

                    sender.sendMessage(ChatColor.DARK_GREEN + "Starting import");

                    try {
                        Connection qsConn = DriverManager.getConnection("jdbc:sqlite:" + file);
                        Connection csConn = plugin.getConnection();

                        Statement stmt = qsConn.createStatement();
                        ResultSet rs = stmt.executeQuery("SELECT count(*) FROM shops");

                        int total = rs.getInt("count(*)");
                        int cur = 0;

                        stmt = qsConn.createStatement();
                        rs = stmt.executeQuery("SELECT * FROM shops");

                        while (rs.next()) {
                            cur++;
                            sender.sendMessage(ChatColor.DARK_GREEN + "Importing " + cur + " out of " + total);
                            String owner = rs.getString("owner");
                            double price = rs.getDouble("price");

                            YamlConfiguration cfg = new YamlConfiguration();
                            cfg.loadFromString(rs.getString("itemConfig"));
                            ItemStack item = cfg.getItemStack("item");

                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

                            ItemStack inputItem = item.clone();
                            inputItem.setAmount(1);

                            dataOutput.writeObject(inputItem);
                            dataOutput.close();

                            int x = rs.getInt("x");
                            int y = rs.getInt("y");
                            int z = rs.getInt("z");
                            String world = rs.getString("world");
                            String type = (rs.getInt("type") == 1 ? "buy" : "sell");
                            String groupId = item.getType().toString();

                            PreparedStatement pstmt = csConn.prepareStatement("SELECT count(*) FROM shops WHERE world=? AND x=? AND y=? AND z=?");
                            pstmt.setString(1, world);
                            pstmt.setInt(2, x);
                            pstmt.setInt(3, y);
                            pstmt.setInt(4, z);

                            ResultSet rs2 = pstmt.executeQuery();
                            if (rs2.getInt("count(*)") > 0) {
                                pstmt.close();
                                continue;
                            }

                            pstmt = csConn.prepareStatement("INSERT INTO shops (owner, type, item, material, price, world, x, y, z, groupId) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                            pstmt.setString(1, owner);
                            pstmt.setString(2, type);
                            pstmt.setString(3, Base64Coder.encodeLines(outputStream.toByteArray()));
                            pstmt.setString(4, groupId);
                            pstmt.setDouble(5, price);
                            pstmt.setString(6, world);
                            pstmt.setInt(7, x);
                            pstmt.setInt(8, y);
                            pstmt.setInt(9, z);
                            pstmt.setString(10, groupId);
                            pstmt.executeUpdate();

                            pstmt = csConn.prepareStatement("SELECT count(*) FROM groups WHERE groupId=? AND owner=?");
                            pstmt.setString(1, groupId);
                            pstmt.setString(2, owner);
                            rs2 = pstmt.executeQuery();

                            if (rs2.getInt("count(*)") < 1) {
                                pstmt = csConn.prepareStatement("INSERT INTO groups (owner, groupId) VALUES (?, ?)");
                                pstmt.setString(1, owner);
                                pstmt.setString(2,groupId);
                                pstmt.executeUpdate();
                            }

                            pstmt.close();
                        }

                        stmt.close();
                        sender.sendMessage(ChatColor.DARK_GREEN + "Import finished.");
                    } catch (Exception e) {
                        sender.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                        plugin.getLogger().severe("customshop import: " + e.getClass().getName() + ": " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            }.runTaskAsynchronously(plugin);
            return true;
        }
        

        if (args[0].equals("stock")) {
            if (!sender.hasPermission("customshops.create")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }

            int pageNum = 1;

            if (args.length > 1) {
                try {
                    pageNum = Integer.parseInt(args[1]);
                } catch (NumberFormatException ignored) {}
                if (pageNum < 1) pageNum = 1;
            }

            try {
                Connection c = plugin.getConnection();
                PreparedStatement pstmt = c.prepareStatement("SELECT * FROM shops WHERE owner=?");
                pstmt.setString(1, player.getUniqueId().toString());

                ResultSet rs = pstmt.executeQuery();
                List<Map> shops = new LinkedList<>();

                while (rs.next()) {
                    World curWorld = Bukkit.getWorld(rs.getString("world"));

                    if (curWorld == null) continue; // world no longer exists or isn't loaded

                    Block curBlock = curWorld.getBlockAt(rs.getInt("x"), rs.getInt("y"), rs.getInt("z"));
                    BlockState state = curBlock.getState();
                    if (!(state instanceof Chest)) continue;

                    ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(rs.getString("item")));
                    BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
                    ItemStack item = (ItemStack) dataInput.readObject();
                    dataInput.close();

                    int stock = Utils.countItems(((Chest) state).getInventory(), item);
                    Map curMap = new HashMap<>();
                    curMap.put("item", item);
                    curMap.put("stock", stock);
                    shops.add(curMap);
                }

                pstmt.close();

                if (shops.size() < 1) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You don't own any shops.");
                    return true;
                }

                shops.sort(Comparator.comparingInt(m -> ((int) m.get("stock"))));

                List<TextComponent> messages = new LinkedList<>();

                for (Map curMap : shops) {
                    ItemStack curItem = ((ItemStack) curMap.get("item"));
                    int amt = ((int) curMap.get("stock"));

                    String name = Utils.materialName(curItem.getType());

                    TextComponent message = new TextComponent("Item: ");
                    message.setColor(ChatColor.GREEN.asBungee());

                    TextComponent itemName = new TextComponent(name);
                    itemName.setColor(ChatColor.YELLOW.asBungee());
                    itemName.setHoverEvent(plugin.getItemHover(CraftItemStack.asNMSCopy(curItem)));

                    TextComponent stock = new TextComponent(" Stock: ");
                    stock.setColor(ChatColor.GREEN.asBungee());

                    TextComponent stockAmt = new TextComponent("" + amt);
                    stockAmt.setColor(ChatColor.YELLOW.asBungee());

                    message.addExtra(itemName);
                    message.addExtra(stock);
                    message.addExtra(stockAmt);

                    messages.add(message);
                }

                List<BaseComponent> pages = plugin.pageify(messages, pageNum, "/shop stock");

                for (BaseComponent page : pages) player.spigot().sendMessage(page);
                return true;

            } catch (Exception e) {
                player.sendMessage(ChatColor.DARK_RED + "Error querying database.");
                plugin.getLogger().severe("/shop empty: " + e.getClass().getName() + ": " + e.getMessage());
                e.printStackTrace();
                return true;
            }
        }

        sender.sendMessage(ChatColor.DARK_GREEN + "CustomShops commands:");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop create <buy | sell> <price> - " + ChatColor.GOLD + "Creates a shop.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop remove - " + ChatColor.GOLD + "Removes a shop.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop cancel - " + ChatColor.GOLD + "Cancels any pending actions.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop find <item> - Displays the closest shops selling/buying <item>.");
        if (sender.hasPermission("customshops.admin")) sender.sendMessage(ChatColor.DARK_GREEN + "/shop acreate <buy | sell> <price> - " + ChatColor.GOLD + "Creates an AdminShop.");
        setHelp(sender);
        return true;

    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) {
            if (sender.hasPermission("customshops.admin")) tabComplete.add("create");
            tabComplete.add("remove");
            tabComplete.add("cancel");
            tabComplete.add("find");
            tabComplete.add("closest");
            tabComplete.add("reset");
            tabComplete.add("set");
        }

        if (args.length == 2) {
            if (args[0].equals("remove") && sender.hasPermission("customshops.admin")) tabComplete.addAll(plugin.shopIds);
            if (args[0].equals("create") && sender.hasPermission("customshops.admin")) tabComplete.add("<shopname>");
            if (args[0].equals("find") || args[0].equals("closest")) tabComplete.add("<item>");
            if (args[0].equals("set")) {
                tabComplete.add("price");
                tabComplete.add("limit");
                tabComplete.add("modifier");
                tabComplete.add("time");
                tabComplete.add("group");
                tabComplete.add("type");
                tabComplete.add("owner");
                if (sender.hasPermission("customshops.admin")) tabComplete.add("desc");
            }
        }

        if (args.length == 3) {
            if (args[0].equals("create") && sender.hasPermission("customshops.admin")) return null;
            if (args[0].equals("set")) {
                if (args[1].equals("price")) tabComplete.add("<amount>");
                if (args[1].equals("limit")) tabComplete.add("<amount>");
                if (args[1].equals("modifier")) tabComplete.add("<percentage>");
                if (args[1].equals("time")) tabComplete.add("<time (1s/m/h/d)>");
                if (args[1].equals("group")) tabComplete.add("<group>");
                if (args[1].equals("owner")) tabComplete.add("<owner>");
                if (args[1].equals("type")) {
                    tabComplete.add("buy");
                    tabComplete.add("sell");
                }
                if (args[1].equals("desc") && sender.hasPermission("customshops.admin")) tabComplete.addAll(plugin.shopIds);
            }
        }

        if (args.length == 4) {
            if (args[0].equals("set")  && args[1].equals("modifier")) tabComplete.add("<amount>");
            if (args[0].equals("create") && sender.hasPermission("customshops.admin")) tabComplete.add("<description>");
            if (args[0].equals("desc") && sender.hasPermission("customshops.admin")) tabComplete.add("<description");
        }

        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<String>()) : null;
    }

    private void setHelp(CommandSender sender) {
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop set price <amount> - " + ChatColor.GOLD + "Changes the price on a shop.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop set limit <amount> - " + ChatColor.GOLD + "Sets the maximum amount a player can buy from / sell to a shop.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop set modifier <percentage> <amount> - " + ChatColor.GOLD + "Sets the percentage the price should decrease / increase to after a certain amount has been sold / purchased.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop set time <time (1s/m/h/d)> - " + ChatColor.GOLD + "Sets how often the limits / price should reset.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop set group <group> - " + ChatColor.GOLD + "Sets the group for a chest.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop set desc <shopid> <description - " + ChatColor.GOLD + "Sets the description for a shop region.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop set type <buy/sell> - " + ChatColor.GOLD + "Sets whether the shop buys or sells items.");
        sender.sendMessage(ChatColor.DARK_GREEN + "/shop set owner <owner> - " + ChatColor.GOLD + "Changes the owner of a shop.");
    }

    private List<ItemStack> removeItems(Inventory inv, ItemStack item, int amountToRemove) {
        ItemStack[] contents = inv.getContents();
        List<ItemStack> removedItems = new ArrayList<>();
        int curAmount = 0;
        int slot = 0;

        for (ItemStack curItem : contents) {
            slot++;
            if (curItem == null || !Utils.sameItem(item, curItem)) continue;
            if ((curItem.getAmount() + curAmount) <= amountToRemove) {
                curAmount += curItem.getAmount();
                if (inv instanceof PlayerInventory && slot > 36) {
                    inv.setItem((slot - 1), null);
                } else {
                    inv.removeItem(curItem);
                }
                removedItems.add(curItem);
            } else {
                int removeAmount = (amountToRemove - curAmount);
                int newSize = curItem.getAmount() - removeAmount;
                curItem.setAmount(newSize);
                ItemStack clonedItem = curItem.clone();
                clonedItem.setAmount(removeAmount);
                if (inv instanceof PlayerInventory && slot > 36) {
                    inv.setItem((slot - 1), clonedItem);
                }
                removedItems.add(clonedItem);
                curAmount += removeAmount;
            }

            if (curAmount == amountToRemove) break;
        }

        return removedItems;
    }


    /*
     * from https://github.com/KaiKikuchi/QuickShop/blob/master/src/main/java/org/maxgamer/quickshop/Command/QS.java#L614
     */
     Location lookAt(Location loc, Location lookat) {
        // Clone the loc to prevent applied changes to the input loc
        loc = loc.clone();
        // Values of change in distance (make it relative)
        double dx = lookat.getX() - loc.getX();
        double dy = lookat.getY() - loc.getY();
        double dz = lookat.getZ() - loc.getZ();
        // Set yaw
        if (dx != 0) {
            // Set yaw start value based on dx
            if (dx < 0) {
                loc.setYaw((float) (1.5 * Math.PI));
            } else {
                loc.setYaw((float) (0.5 * Math.PI));
            }
            loc.setYaw(loc.getYaw() - (float) Math.atan(dz / dx));
        } else if (dz < 0) {
            loc.setYaw((float) Math.PI);
        }
        // Get the distance from dx/dz
        double dxz = Math.sqrt(Math.pow(dx, 2) + Math.pow(dz, 2));
        float pitch = (float) -Math.atan(dy / dxz);
        // Set values, convert to degrees
        // Minecraft yaw (vertical) angles are inverted (negative)
        loc.setYaw(-loc.getYaw() * 180f / (float) Math.PI + 360);
        // But pitch angles are normal
        loc.setPitch(pitch * 180f / (float) Math.PI);
        return loc;
    }
}
