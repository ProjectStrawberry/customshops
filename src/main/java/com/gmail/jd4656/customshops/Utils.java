package com.gmail.jd4656.customshops;

import com.gmail.jd4656.customshops.types.Transaction;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class Utils {
    public static Material[] validShopMaterials = {Material.CHEST, Material.BARREL, Material.SHULKER_BOX, Material.BLACK_SHULKER_BOX, Material.BLUE_SHULKER_BOX,
    Material.BROWN_SHULKER_BOX, Material.CYAN_SHULKER_BOX, Material.GRAY_SHULKER_BOX, Material.GREEN_SHULKER_BOX, Material.LIGHT_BLUE_SHULKER_BOX,
    Material.LIGHT_GRAY_SHULKER_BOX, Material.LIME_SHULKER_BOX, Material.MAGENTA_SHULKER_BOX, Material.ORANGE_SHULKER_BOX, Material.PINK_SHULKER_BOX,
    Material.PURPLE_SHULKER_BOX, Material.RED_SHULKER_BOX, Material.WHITE_SHULKER_BOX, Material.YELLOW_SHULKER_BOX};

    public static ItemStack setDisplayName(ItemStack item, String name) {
        return setDisplayName(item, name, null);
    }
    public static ItemStack setDisplayName(ItemStack item, String name, String key) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);

        if (key != null) {
            meta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, key), PersistentDataType.INTEGER, 1);
        }

        item.setItemMeta(meta);
        return item;
    }

    public static boolean validShopMaterial(Material material) {
        for (Material curMaterial : validShopMaterials) {
            if (curMaterial == material) return true;
        }

         return false;
    }

    public static String materialName(Material material) {
        String name = material.name().replace("_", " ");
        String[] split = name.split(" ");

        for (int i=0; i<split.length; i++) {
            split[i] = Character.toUpperCase(split[i].charAt(0)) + split[i].substring(1).toLowerCase();
        }

        return String.join(" ", split);
    }

    public static ItemStack itemFromBase64(String base64) {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(base64));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack item = (ItemStack) dataInput.readObject();
            dataInput.close();

            return item;
        } catch (IOException | ClassNotFoundException e) {
            return null;
        }
    }

    public static String itemToBase64(ItemStack item) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            ItemStack inputItem = item.clone();
            inputItem.setAmount(1);

            dataOutput.writeObject(inputItem);
            dataOutput.close();

            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (IOException e) {
            return null;
        }
    }

    public static boolean sameItem(ItemStack item1, ItemStack item2) {
        ItemStack clonedItem1 = item1.clone();
        ItemStack clonedItem2 = item2.clone();
        clonedItem1.setAmount(1);
        clonedItem2.setAmount(1);

        if (clonedItem1.hasItemMeta() && clonedItem2.hasItemMeta()) {
            ItemMeta meta1 = clonedItem1.getItemMeta();
            ItemMeta meta2 = clonedItem2.getItemMeta();

            if ((meta1.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS) && meta1.hasDisplayName()) && (meta2.hasItemFlag(ItemFlag.HIDE_POTION_EFFECTS) && meta2.hasDisplayName())) {
                if (meta1.getDisplayName().equals("Indefinable Distillate") && meta2.getDisplayName().equals("Indefinable Distillate")) return true;
                if (meta1.getDisplayName().equals("Indefinable Brew") && meta2.getDisplayName().equals("Indefinable Brew")) return true;
            }
        }

        return (clonedItem1.hashCode() == clonedItem2.hashCode());
    }

    public static int freeSpace(Inventory inv, ItemStack item) {
        int free = 0;

        for (ItemStack curItem : inv.getStorageContents()) {
            if (curItem == null) {
                free += item.getMaxStackSize();
                continue;
            }
            if (!Utils.sameItem(curItem, item)) continue;
            free += (curItem.getMaxStackSize() - curItem.getAmount());
        }

        return free;
    }

    public static int countItems(Inventory inv, ItemStack item) {
        int count = 0;

        for (ItemStack curItem : inv) {
            if (curItem == null) continue;
            if (Utils.sameItem(curItem, item)) {
                count += curItem.getAmount();
            }
        }

        return count;
    }

    public static List<ItemStack> removeItems(Inventory inv, ItemStack item, int amountToRemove) {
        ItemStack[] contents = inv.getContents();
        List<ItemStack> removedItems = new ArrayList<>();
        int curAmount = 0;
        int slot = 0;

        for (ItemStack curItem : contents) {
            slot++;
            if (curItem == null || !Utils.sameItem(item, curItem)) continue;
            if ((curItem.getAmount() + curAmount) <= amountToRemove) {
                curAmount += curItem.getAmount();
                if (inv instanceof PlayerInventory && slot > 36) {
                    inv.setItem((slot - 1), null);
                } else {
                    inv.removeItem(curItem);
                }
                removedItems.add(curItem);
            } else {
                int removeAmount = (amountToRemove - curAmount);
                int newSize = curItem.getAmount() - removeAmount;
                curItem.setAmount(newSize);
                ItemStack clonedItem = curItem.clone();
                clonedItem.setAmount(removeAmount);
                if (inv instanceof PlayerInventory && slot > 36) {
                    inv.setItem((slot - 1), clonedItem);
                }
                removedItems.add(clonedItem);
                curAmount += removeAmount;
            }

            if (curAmount == amountToRemove) break;
        }

        return removedItems;
    }

    public static CompletableFuture<List<Transaction>> getTransactions(String ownerId, int page) {
        CompletableFuture<List<Transaction>> completableFuture = new CompletableFuture<>();

        int pageStart = (page * 21) - 21;
        int pageEnd = pageStart + 21;

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = CustomShops.plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT * FROM logs WHERE owner=? ORDER BY date DESC LIMIT ?,?");
                    pstmt.setString(1, ownerId);
                    pstmt.setInt(2, pageStart);
                    pstmt.setInt(3, pageEnd);

                    List<Transaction> transactions = new ArrayList<>();

                    ResultSet rs = pstmt.executeQuery();
                    while (rs.next()) {
                        Transaction curTransaction = new Transaction(rs.getString("id"));
                        curTransaction.setUuid(rs.getString("uuid"));
                        curTransaction.setAction(rs.getString("action"));
                        curTransaction.setItem(Utils.itemFromBase64(rs.getString("item")));
                        curTransaction.setAmount(rs.getInt("amount"));
                        curTransaction.setShopPrice(rs.getDouble("shopPrice"));
                        curTransaction.setTotal(rs.getDouble("money"));
                        curTransaction.setUnread(rs.getInt("unread") == 1);
                        curTransaction.setWorldName(rs.getString("world"));
                        curTransaction.setX(rs.getInt("x"));
                        curTransaction.setY(rs.getInt("y"));
                        curTransaction.setZ(rs.getInt("z"));
                        curTransaction.setDate(rs.getLong("date"));

                        transactions.add(curTransaction);
                    }

                    pstmt.close();

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(transactions);
                        }
                    }.runTask(CustomShops.plugin);
                } catch (Exception e) {
                    CustomShops.plugin.getLogger().warning("Error getting transaction history:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(CustomShops.plugin);

        return completableFuture;
    }

    public static CompletableFuture<Transaction> getTransaction(String id) {
        CompletableFuture<Transaction> completableFuture = new CompletableFuture<>();

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = CustomShops.plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT * FROM logs WHERE id=?");
                    pstmt.setString(1, id);

                    Transaction transaction = null;
                    ResultSet rs = pstmt.executeQuery();
                    while (rs.next()) {
                        transaction = new Transaction(rs.getString("id"));
                        transaction.setUuid(rs.getString("uuid"));
                        transaction.setAction(rs.getString("action"));
                        transaction.setItem(Utils.itemFromBase64(rs.getString("item")));
                        transaction.setAmount(rs.getInt("amount"));
                        transaction.setShopPrice(rs.getDouble("shopPrice"));
                        transaction.setTotal(rs.getDouble("money"));
                        transaction.setUnread(rs.getInt("unread") == 1);
                        transaction.setWorldName(rs.getString("world"));
                        transaction.setX(rs.getInt("x"));
                        transaction.setY(rs.getInt("y"));
                        transaction.setZ(rs.getInt("z"));
                        transaction.setDate(rs.getLong("date"));
                    }

                    pstmt.close();

                    Transaction finalTransaction = transaction;

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(finalTransaction);
                        }
                    }.runTask(CustomShops.plugin);
                } catch (Exception e) {
                    CustomShops.plugin.getLogger().warning("Error getting transaction (id: " + id + "):");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(CustomShops.plugin);

        return completableFuture;
    }
}
