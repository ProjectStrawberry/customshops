package com.gmail.jd4656.customshops.types;

import com.gmail.jd4656.customshops.CustomShops;
import com.gmail.jd4656.customshops.Utils;
import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class Shop {
    private String owner;
    private String type;
    private String groupId;
    private String permission;

    private double price;

    private ItemStack item;
    private Material material;
    private Location shopLocation;


    public Shop(String owner, String type, String groupId, ItemStack item, double price, Location shopLocation, String permission) {
        this.owner = owner;
        this.type = type;
        this.groupId = groupId;
        this.item = item;
        this.material = item.getType();
        this.price = price;
        this.shopLocation = shopLocation;
        this.permission = permission;
    }

    public Location getShopLocation() {
        return this.shopLocation;
    }

    public ItemStack getItem() {
        return this.item.clone();
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getGroupId() {
        return this.groupId;
    }

    public void setGroupId(String newGroupId) {
        this.groupId = newGroupId;
        this.save();
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner) {
        String oldOwner = this.owner;
        String newOwner = owner;
        String groupId = this.groupId;

        this.owner = owner;

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = CustomShops.plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM shops WHERE groupId=? AND owner=?");
                    pstmt.setString(1, groupId);
                    pstmt.setString(2, oldOwner);

                    ResultSet rs = pstmt.executeQuery();

                    if (rs.getInt("count(*)") < 2) {
                        pstmt = c.prepareStatement("UPDATE groups SET owner=? WHERE groupId=? AND owner=?");
                        pstmt.setString(1, newOwner);
                        pstmt.setString(2, groupId);
                        pstmt.setString(3, oldOwner);
                        pstmt.executeUpdate();
                    } else {
                        pstmt = c.prepareStatement("INSERT INTO groups (groupId, owner) VALUES (?, ?)");
                        pstmt.setString(1, groupId);
                        pstmt.setString(2, newOwner);
                        pstmt.executeUpdate();
                    }

                    pstmt.close();
                } catch (Exception e) {
                    CustomShops.plugin.getLogger().info("shop setOwner:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(CustomShops.plugin);
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void logTransaction(Player player, String action, int amount, double money) {

        String uuid = player.getUniqueId().toString();
        String owner = this.getOwner();
        String shopType = this.getType();
        double shopPrice = this.getPrice();

        String worldName = this.shopLocation.getWorld().getName();

        int x = this.shopLocation.getBlockX();
        int y = this.shopLocation.getBlockY();
        int z = this.shopLocation.getBlockZ();

        Container container = (Container) this.shopLocation.getBlock().getState();
        if (!owner.equals("admin")) {
            Player owningPlayer = Bukkit.getPlayer(UUID.fromString(owner));
            if (owningPlayer != null && owningPlayer.isOnline()) {
                if (this.type.equals("buy") && Utils.freeSpace(container.getInventory(), this.item) < 1) {
                    owningPlayer.sendMessage(ChatColor.DARK_PURPLE + "Your shop at " + x + ", " + y + ", " + z + " has run out of space.");
                }

                if (this.type.equals("sell") && Utils.countItems(container.getInventory(), this.item) < 1) {
                    owningPlayer.sendMessage(ChatColor.DARK_PURPLE + "Your shop at " + x + ", " + y + ", " + z + " is out of stock.");
                }
            }
        }

        String itemStr = Utils.itemToBase64(this.item);

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = CustomShops.plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("INSERT INTO logs (id, uuid, owner, shopType, shopPrice, action, unread, amount, money, item, world, x, y, z, date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    pstmt.setString(1, UUID.randomUUID().toString());
                    pstmt.setString(2, uuid);
                    pstmt.setString(3, owner);
                    pstmt.setString(4, shopType);
                    pstmt.setDouble(5, shopPrice);
                    pstmt.setString(6, action);
                    pstmt.setInt(7, 1);
                    pstmt.setInt(8, amount);
                    pstmt.setDouble(9, money);
                    pstmt.setString(10, itemStr);
                    pstmt.setString(11, worldName);
                    pstmt.setInt(12, x);
                    pstmt.setInt(13, y);
                    pstmt.setInt(14, z);
                    pstmt.setLong(15, System.currentTimeMillis());
                    pstmt.executeUpdate();
                    pstmt.close();
                } catch (Exception e) {
                    CustomShops.plugin.getLogger().info("Error logging shop transaction:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(CustomShops.plugin);
    }

    public void updateSign() {
        Container container = (Container) shopLocation.getBlock().getState();
        if (container.getInventory().getHolder() instanceof DoubleChest) {
            DoubleChest doubleChest = (DoubleChest) container.getInventory().getHolder();

            this.updateSign(((Chest) doubleChest.getLeftSide()).getLocation());
            this.updateSign(((Chest) doubleChest.getRightSide()).getLocation());
        } else {
            this.updateSign(shopLocation);
        }
    }

    public void updateSign(Location loc) {
        Block block = loc.getBlock();
        Container container = (Container) block.getState();
        BlockFace[] blockFaces = new BlockFace[]{BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};

        for (BlockFace curFace : blockFaces) {
            Block curBlock = block.getRelative(curFace);
            if (!(curBlock.getBlockData() instanceof WallSign)) continue;
            WallSign wallSign = (WallSign) curBlock.getBlockData();
            if (!curBlock.getRelative(wallSign.getFacing().getOppositeFace()).getLocation().equals(block.getLocation())) continue;

            OfflinePlayer offlinePlayer = null;
            if (!owner.equals("admin")) offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(owner));
            Sign sign = (Sign) curBlock.getState();

            int amount = 10000;
            if (offlinePlayer != null) {
                amount = (type.equals("sell") ? Utils.countItems(container.getInventory(), item) : Utils.freeSpace(container.getInventory(), item));
            }

            sign.setLine(0, ChatColor.RED + (offlinePlayer == null ? "AdminShop" : offlinePlayer.getName()));
            sign.setLine(1, (type.equals("sell") ? ChatColor.AQUA + "Selling" : ChatColor.LIGHT_PURPLE + "Buying") + " " + amount);
            sign.setLine(2, Utils.materialName(item.getType()));
            sign.setLine(3, "$" + String.format("%.2f", price) + " each");
            sign.update();
        }
    }

    public boolean canEdit(Player player) {
        return (player.getUniqueId().toString().equals(this.owner) || player.hasPermission("customshops.admin"));
    }

    public CompletableFuture<Double> getTotalSalesEarned() {
        CompletableFuture<Double> completableFuture = new CompletableFuture<>();

        String owner = this.owner;
        String action = (this.type.equals("buy") ? "SELL" : "BUY");
        String itemStr = Utils.itemToBase64(this.item);
        String world = this.shopLocation.getWorld().getName();

        int x = this.shopLocation.getBlockX();
        int y = this.shopLocation.getBlockY();
        int z = this.shopLocation.getBlockZ();

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = CustomShops.plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT SUM(money) FROM logs WHERE owner=? AND action=? AND item=? AND world=? AND x=? AND y=? AND z=?");
                    pstmt.setString(1, owner);
                    pstmt.setString(2, action);
                    pstmt.setString(3, itemStr);
                    pstmt.setString(4, world);
                    pstmt.setInt(5, x);
                    pstmt.setInt(6, y);
                    pstmt.setInt(7, z);

                    double total = 0;

                    ResultSet rs = pstmt.executeQuery();
                    while (rs.next()) total = rs.getDouble("SUM(money)");

                    pstmt.close();

                    double finalTotal = total;

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(finalTotal);
                        }
                    }.runTask(CustomShops.plugin);
                } catch (Exception e) {
                    CustomShops.plugin.getLogger().warning("Error getting total sales:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(CustomShops.plugin);

        return completableFuture;
    }

    public CompletableFuture<Integer> getTotalSales() {
        CompletableFuture<Integer> completableFuture = new CompletableFuture<>();

        String owner = this.owner;
        String action = (this.type.equals("buy") ? "SELL" : "BUY");
        String itemStr = Utils.itemToBase64(this.item);
        String world = this.shopLocation.getWorld().getName();

        int x = this.shopLocation.getBlockX();
        int y = this.shopLocation.getBlockY();
        int z = this.shopLocation.getBlockZ();

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = CustomShops.plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("SELECT COUNT(*) FROM logs WHERE owner=? AND action=? AND item=? AND world=? AND x=? AND y=? AND z=?");
                    pstmt.setString(1, owner);
                    pstmt.setString(2, action);
                    pstmt.setString(3, itemStr);
                    pstmt.setString(4, world);
                    pstmt.setInt(5, x);
                    pstmt.setInt(6, y);
                    pstmt.setInt(7, z);

                    int total = 0;

                    ResultSet rs = pstmt.executeQuery();
                    while (rs.next()) total = rs.getInt("COUNT(*)");

                    pstmt.close();

                    int finalTotal = total;

                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            completableFuture.complete(finalTotal);
                        }
                    }.runTask(CustomShops.plugin);
                } catch (Exception e) {
                    CustomShops.plugin.getLogger().warning("Error getting total sales:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(CustomShops.plugin);

        return completableFuture;
    }

    public void save() {
        String itemStr = Utils.itemToBase64(item);
        String worldName = shopLocation.getWorld().getName();

        int x = shopLocation.getBlockX();
        int y = shopLocation.getBlockY();
        int z = shopLocation.getBlockZ();

        String permission = this.getPermission();
        String type = this.getType();

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = CustomShops.plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("DELETE FROM shops WHERE world=? AND x=? AND y=? AND z=?");

                    pstmt.setString(1, worldName);
                    pstmt.setInt(2, x);
                    pstmt.setInt(3, y);
                    pstmt.setInt(4, z);
                    pstmt.executeUpdate();

                    pstmt = c.prepareStatement("INSERT INTO shops (owner, type, item, material, price, permission, world, x, y, z, groupId) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    pstmt.setString(1, owner);
                    pstmt.setString(2, type);
                    pstmt.setString(3, itemStr);
                    pstmt.setString(4, groupId);
                    pstmt.setDouble(5, price);
                    pstmt.setString(6, permission);
                    pstmt.setString(7, worldName);
                    pstmt.setInt(8, x);
                    pstmt.setInt(9, y);
                    pstmt.setInt(10, z);
                    pstmt.setString(11, groupId);
                    pstmt.executeUpdate();

                    pstmt = c.prepareStatement("SELECT COUNT(*) FROM groups WHERE groupId=? AND owner=?");
                    pstmt.setString(1, groupId);
                    pstmt.setString(2, owner);

                    ResultSet rs = pstmt.executeQuery();
                    if (rs.getInt("COUNT(*)") < 1) {
                        pstmt = c.prepareStatement("INSERT INTO groups (groupId, owner) VALUES (?, ?)");
                        pstmt.setString(1, groupId);
                        pstmt.setString(2, owner);
                        pstmt.executeUpdate();
                    }

                    pstmt.close();
                } catch (Exception e) {
                    CustomShops.plugin.getLogger().warning("Error saving shop data:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(CustomShops.plugin);
    }

    public String getPermission() {
        return (permission == null ? "" : permission);
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
}
