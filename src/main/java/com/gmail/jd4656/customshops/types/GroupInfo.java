package com.gmail.jd4656.customshops.types;

public class GroupInfo {
    private int sellAmount;
    private int limit;
    private int priceModifierAmount;
    private int curAmount = 0;
    private double priceModifier;
    private long resetsAt;

    public GroupInfo() {

    }


    public int getSellAmount() {
        return sellAmount;
    }

    public void setSellAmount(int sellAmount) {
        this.sellAmount = sellAmount;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPriceModifierAmount() {
        return priceModifierAmount;
    }

    public void setPriceModifierAmount(int priceModifierAmount) {
        this.priceModifierAmount = priceModifierAmount;
    }

    public int getCurAmount() {
        return curAmount;
    }

    public void setCurAmount(int curAmount) {
        this.curAmount = curAmount;
    }

    public double getPriceModifier() {
        return priceModifier;
    }

    public void setPriceModifier(double priceModifier) {
        this.priceModifier = priceModifier;
    }

    public long getResetsAt() {
        return resetsAt;
    }

    public void setResetsAt(long resetsAt) {
        this.resetsAt = resetsAt;
    }
}
