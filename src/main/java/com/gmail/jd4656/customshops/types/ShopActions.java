package com.gmail.jd4656.customshops.types;

public enum ShopActions {
    CHANGE_TYPE,
    CHANGE_OWNER,
    CHANGE_PRICE,
    SET_LIMIT,
    SET_MODIFIER,
    SET_PERMISSION,
    RESET_TIME,
    RESET_MODIFIERS,
    SET_GROUP,
    CREATE,
    TRANSACTION,
    FIX
}
