package com.gmail.jd4656.customshops.types;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class ShopAction {
    private ShopActions action;
    private Location shopLocation;
    private Location playerLocation;
    private String[] actionArgs;
    private ItemStack item;

    public ShopActions getAction() {
        return this.action;
    }

    public void setShopLocation(Location loc) {
        this.shopLocation = loc;
    }

    public Location getShopLocation() {
        return this.shopLocation;
    }

    public ItemStack getItem() {
        return this.item;
    }

    public void setItem(ItemStack item) {
        this.item = item.clone();
    }

    public void setActionArgs(String args) {
        this.actionArgs = new String[]{args};
    }

    public void setActionArgs(String[] args) {
        this.actionArgs = args;
    }

    public void setAction(ShopActions action) {
        this.action = action;
    }

    public void setPlayerLocation(Location location) {
        this.playerLocation = location;
    }

    public Location getPlayerLocation() {
        return playerLocation;
    }

    public String getActionArg() {
        return actionArgs[0];
    }

    public String[] getActionArgs() {
        return actionArgs;
    }
}
