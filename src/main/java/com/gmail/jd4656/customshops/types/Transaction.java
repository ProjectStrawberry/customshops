package com.gmail.jd4656.customshops.types;

import com.gmail.jd4656.customshops.CustomShops;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class Transaction {
    private String id;
    private String uuid;
    private String action;

    private ItemStack item;

    private String worldName;
    private int x,y,z;

    private long date;
    private int amount;
    private double shopPrice;
    private double total;

    private boolean unread;

    public Transaction(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public ItemStack getItem() {
        return item;
    }

    public void setItem(ItemStack item) {
        this.item = item;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getShopPrice() {
        return shopPrice;
    }

    public void setShopPrice(double shopPrice) {
        this.shopPrice = shopPrice;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void markRead() {
        String id = this.id;
        this.unread = false;

        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    Connection c = CustomShops.plugin.getConnection();
                    PreparedStatement pstmt = c.prepareStatement("UPDATE logs SET unread=0 WHERE id=?");
                    pstmt.setString(1, id);
                    pstmt.executeUpdate();
                    pstmt.close();
                } catch (Exception e) {
                    CustomShops.plugin.getLogger().warning("Error setting transaction as read:");
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(CustomShops.plugin);
    }

    public boolean isUnread() {
        return unread;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    public String getWorldName() {
        return worldName;
    }

    public void setWorldName(String worldName) {
        this.worldName = worldName;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
