package com.gmail.jd4656.customshops.ui;

import com.gmail.jd4656.customshops.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class MainPage {
    Inventory inv;

    public MainPage(Player player) {
        inv = Bukkit.createInventory(null, 27, ChatColor.GOLD + "" + ChatColor.BOLD + "Shop Menu");

        ItemStack fillerItem = Utils.setDisplayName(new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1), " ", "filler");
        ItemStack recentTransactions = Utils.setDisplayName(new ItemStack(Material.CHEST, 1), ChatColor.GOLD + "[" + ChatColor.GREEN + "Recent Transactions" + ChatColor.GOLD + "]", "recentTransactions");
        ItemStack yourShops = Utils.setDisplayName(new ItemStack(Material.CHEST, 1), ChatColor.GOLD + "[" + ChatColor.BLUE + "Your Shops" + ChatColor.GOLD + "]", "yourShops");
        ItemStack findAShop = Utils.setDisplayName(new ItemStack(Material.CHEST, 1), ChatColor.GOLD + "[" + ChatColor.YELLOW + "Find a Shop" + ChatColor.GOLD + "]", "findAShop");

        for (int i=0; i<27; i++) inv.setItem(i, fillerItem);
        inv.setItem(12, recentTransactions);
        inv.setItem(13, yourShops);
        inv.setItem(14, findAShop);

        player.openInventory(inv);
    }
}
