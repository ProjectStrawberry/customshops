package com.gmail.jd4656.customshops.ui;

import com.gmail.jd4656.customshops.CustomShops;
import com.gmail.jd4656.customshops.Utils;
import com.gmail.jd4656.customshops.types.Shop;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class YourShops {
    Inventory inv;

    YourShops(Player player, int page) {
        inv = Bukkit.createInventory(null, 54, ChatColor.GOLD + "[" + ChatColor.DARK_BLUE + "Your Shops" + ChatColor.GOLD + "]");

        ItemStack fillerItem = Utils.setDisplayName(new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1), " ", "filler");
        ItemStack goBack = Utils.setDisplayName(new ItemStack(Material.MAGENTA_GLAZED_TERRACOTTA, 1), ChatColor.RED + "Return to Main Menu", "mainmenu");

        for (int i=0; i<54; i++) inv.setItem(i, fillerItem);

        inv.setItem(4, goBack);

        int pageStart = (page * 21) - 21;
        int pageEnd = pageStart + 21;

        List<Shop> shops = CustomShops.plugin.getShops().stream().filter(shop -> shop.getOwner().equals(player.getUniqueId().toString())).collect(Collectors.toList());

        int shopCount = 1;
        int invPos = 10;

        for (Shop shop: shops) {
            if (shopCount >= pageStart && shopCount <= pageEnd) {
                Location loc = shop.getShopLocation();
                ItemStack curItem = new ItemStack(Material.CHEST, 1);
                ItemMeta curMeta = curItem.getItemMeta();

                curMeta.setDisplayName(ChatColor.GOLD + "[" + ChatColor.AQUA + "Shop Information" + ChatColor.GOLD + "]");
                List<String> lore = new ArrayList<>();

                if (shop.getType().equals("buy")) {
                    lore.add(ChatColor.GOLD + "[" + ChatColor.RED + "Buying" + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.materialName(shop.getItem().getType()));
                } else {
                    lore.add(ChatColor.GOLD + "[" + ChatColor.AQUA + "Selling" + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.materialName(shop.getItem().getType()));
                }

                lore.add(ChatColor.GOLD + "[" + ChatColor.GREEN + "Price" + ChatColor.GOLD + "] " + ChatColor.GRAY + "$" + shop.getPrice());
                //lore.add(ChatColor.GOLD + "[" + ChatColor.BLUE + (shop.getType().equals("buy") ? "Purchases" : "Sales") + ChatColor.GOLD + "] " + ChatColor.GRAY + "<amount>");
                lore.add(ChatColor.GOLD + "[" + ChatColor.GREEN + "Location" + ChatColor.GOLD + "] " + ChatColor.GRAY + "X " + loc.getBlockX() + " Y " + loc.getBlockY() + " Z " + loc.getBlockZ());

                curMeta.setLore(lore);

                PersistentDataContainer container = curMeta.getPersistentDataContainer();

                container.set(new NamespacedKey(CustomShops.plugin, "shopDetails"), PersistentDataType.INTEGER, 1);
                container.set(new NamespacedKey(CustomShops.plugin, "world"), PersistentDataType.STRING, loc.getWorld().getName());
                container.set(new NamespacedKey(CustomShops.plugin, "x"), PersistentDataType.INTEGER, loc.getBlockX());
                container.set(new NamespacedKey(CustomShops.plugin, "y"), PersistentDataType.INTEGER, loc.getBlockY());
                container.set(new NamespacedKey(CustomShops.plugin, "z"), PersistentDataType.INTEGER, loc.getBlockZ());

                curItem.setItemMeta(curMeta);

                inv.setItem(invPos, curItem);

                if (invPos == 16 || invPos == 25) invPos += 2;
                invPos++;
            }

            shopCount++;
        }

        ItemStack previous = Utils.setDisplayName(new ItemStack(Material.ARROW, 1), ChatColor.AQUA + "Previous Page", "yourShops");
        ItemMeta previousMeta = previous.getItemMeta();
        previousMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "page"), PersistentDataType.INTEGER, (page == 1 ? page : page - 1));
        previous.setItemMeta(previousMeta);

        ItemStack next = Utils.setDisplayName(new ItemStack(Material.ARROW, 1), ChatColor.AQUA + "Next Page", "yourShops");
        ItemMeta nextMeta = next.getItemMeta();
        nextMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "page"), PersistentDataType.INTEGER, page + 1);
        next.setItemMeta(nextMeta);

        ItemStack currentPage = Utils.setDisplayName(new ItemStack(Material.OAK_SIGN, 1), ChatColor.AQUA + "Page " + page, "filler");


        inv.setItem(45, previous);
        inv.setItem(49, currentPage);
        inv.setItem(53, next);

        player.openInventory(inv);
    }
}
