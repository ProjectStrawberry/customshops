package com.gmail.jd4656.customshops.ui;

import com.gmail.jd4656.customshops.CustomShops;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class UIEvents implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player)) return;
        ItemStack clickedItem = event.getCurrentItem();
        Player player = (Player) event.getWhoClicked();
        if (clickedItem != null && clickedItem.hasItemMeta()) {
            ItemMeta clickedMeta = clickedItem.getItemMeta();
            PersistentDataContainer container = clickedMeta.getPersistentDataContainer();

            if (container.has(new NamespacedKey(CustomShops.plugin, "filler"), PersistentDataType.INTEGER)) {
                event.setCancelled(true);
            }

            if (container.has(new NamespacedKey(CustomShops.plugin, "mainmenu"), PersistentDataType.INTEGER)) {
                event.setCancelled(true);

                new MainPage(player);
            }

            if (container.has(new NamespacedKey(CustomShops.plugin, "transactionId"), PersistentDataType.STRING)) {
                event.setCancelled(true);

                new TransactionDetails(player, container.get(new NamespacedKey(CustomShops.plugin, "transactionId"), PersistentDataType.STRING));
            }

            if (container.has(new NamespacedKey(CustomShops.plugin, "readall"), PersistentDataType.INTEGER)) {
                event.setCancelled(true);

                String uuid = player.getUniqueId().toString();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            Connection c = CustomShops.plugin.getConnection();
                            PreparedStatement pstmt = c.prepareStatement("UPDATE logs SET unread=0 WHERE owner=?");
                            pstmt.setString(1, uuid);
                            pstmt.executeUpdate();
                            pstmt.close();

                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    new RecentTransactions(player, container.getOrDefault(new NamespacedKey(CustomShops.plugin, "page"), PersistentDataType.INTEGER, 1));
                                }
                            }.runTask(CustomShops.plugin);
                        } catch (Exception e) {
                            CustomShops.plugin.getLogger().warning("Error setting transaction as read:");
                            e.printStackTrace();
                        }
                    }
                }.runTaskAsynchronously(CustomShops.plugin);
            }

            if (container.has(new NamespacedKey(CustomShops.plugin, "recentTransactions"), PersistentDataType.INTEGER)) {
                event.setCancelled(true);

                new RecentTransactions(player, container.getOrDefault(new NamespacedKey(CustomShops.plugin, "page"), PersistentDataType.INTEGER, 1));
            }

            if (container.has(new NamespacedKey(CustomShops.plugin, "yourShops"), PersistentDataType.INTEGER)) {
                event.setCancelled(true);

                new YourShops(player, container.getOrDefault(new NamespacedKey(CustomShops.plugin, "page"), PersistentDataType.INTEGER, 1));
            }

            if (container.has(new NamespacedKey(CustomShops.plugin, "shopDetails"), PersistentDataType.INTEGER)) {
                event.setCancelled(true);

                String world = container.get(new NamespacedKey(CustomShops.plugin, "world"), PersistentDataType.STRING);
                int x = container.getOrDefault(new NamespacedKey(CustomShops.plugin, "x"), PersistentDataType.INTEGER, 0);
                int y = container.getOrDefault(new NamespacedKey(CustomShops.plugin, "y"), PersistentDataType.INTEGER, 0);
                int z = container.getOrDefault(new NamespacedKey(CustomShops.plugin, "z"), PersistentDataType.INTEGER, 0);

                Location location = new Location(Bukkit.getWorld(world), x, y, z);

                new ShopDetails(player, location);
            }

            if (container.has(new NamespacedKey(CustomShops.plugin, "findAShop"), PersistentDataType.INTEGER)) {
                event.setCancelled(true);

                // TODO: open the find a shop page
            }
        }
    }
}
