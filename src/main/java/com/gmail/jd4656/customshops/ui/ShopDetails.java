package com.gmail.jd4656.customshops.ui;

import com.gmail.jd4656.customshops.CustomShops;
import com.gmail.jd4656.customshops.Utils;
import com.gmail.jd4656.customshops.types.Shop;
import org.bukkit.*;
import org.bukkit.block.Container;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;

public class ShopDetails {
    Inventory inv;

    ShopDetails(Player player, Location location) {
        inv = Bukkit.createInventory(null, 27, ChatColor.GOLD + "[" + ChatColor.AQUA + "Shop Information" + ChatColor.GOLD + "]");

        ItemStack fillerItem = Utils.setDisplayName(new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1), " ", "filler");
        for (int i=0; i<27; i++) inv.setItem(i, fillerItem);

        ItemStack goBack = Utils.setDisplayName(new ItemStack(Material.MAGENTA_GLAZED_TERRACOTTA, 1), ChatColor.RED + "Return to Your Shops", "yourShops");
        inv.setItem(4, goBack);

        Shop shop = CustomShops.plugin.getShop(location);
        if (shop == null) return;

        shop.getTotalSales().thenAccept(totalSales -> {
           shop.getTotalSalesEarned().thenAccept(totalAmount -> {
               ItemStack shopItem = shop.getItem();
               ItemMeta shopItemMeta = shopItem.getItemMeta();
               shopItemMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "filler"), PersistentDataType.INTEGER, 1);
               shopItem.setItemMeta(shopItemMeta);


               ItemStack infoItem = new ItemStack(Material.OAK_SIGN, 1);
               ItemMeta infoMeta = infoItem.getItemMeta();
               infoMeta.setDisplayName(ChatColor.GOLD + "[" + ChatColor.AQUA + "Shop Information" + ChatColor.GOLD + "]");

               infoMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "filler"), PersistentDataType.INTEGER, 1);

               List<String> lore = new ArrayList<>();
               if (shop.getType().equals("buy")) {
                   lore.add(ChatColor.GOLD + "[" + ChatColor.LIGHT_PURPLE + "Buying" + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.materialName(shop.getItem().getType()));
               } else {
                   lore.add(ChatColor.GOLD + "[" + ChatColor.AQUA + "Selling" + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.materialName(shop.getItem().getType()));
               }
               Container container = (Container) location.getBlock().getState();
               if (shop.getType().equals("buy")) {
                   lore.add(ChatColor.GOLD + "[" + ChatColor.YELLOW + "Remaining Space" + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.freeSpace(container.getInventory(), shop.getItem()));
               } else {
                   lore.add(ChatColor.GOLD + "[" + ChatColor.YELLOW + "Stock" + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.countItems(container.getInventory(), shop.getItem()));
               }
               lore.add(ChatColor.GOLD + "[" + ChatColor.GREEN + "Price" + ChatColor.GOLD + "] " + ChatColor.GRAY + "$" + shop.getPrice());
               lore.add(ChatColor.GOLD + "[" + ChatColor.GREEN + "Total " + (shop.getType().equals("buy") ? "Purchases" : "Sales") + ChatColor.GOLD + "] " + ChatColor.GRAY + totalSales);
               lore.add(ChatColor.GOLD + "----------------------");
               lore.add("" + ChatColor.GOLD + ChatColor.ITALIC + "Total " + (shop.getType().equals("buy") ? "spent" : "earned") + ": " + ChatColor.GRAY + "$" + String.format("%.2f", totalAmount));

               infoMeta.setLore(lore);
               infoItem.setItemMeta(infoMeta);

               inv.setItem(12, shopItem);
               inv.setItem(13, infoItem);

               player.openInventory(inv);
           });
        });
    }
}
