package com.gmail.jd4656.customshops.ui;

import com.gmail.jd4656.customshops.CustomShops;
import com.gmail.jd4656.customshops.Utils;
import com.gmail.jd4656.customshops.types.Transaction;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RecentTransactions {
    Inventory inv;

    public RecentTransactions(Player player, int page) {
        inv = Bukkit.createInventory(null, 54, ChatColor.GOLD + "[" + ChatColor.GREEN + "Recent Transactions" + ChatColor.GOLD + "]");

        ItemStack fillerItem = Utils.setDisplayName(new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1), "", "filler");

        for (int i=0; i<54; i++) inv.setItem(i, fillerItem);

        ItemStack mainMenu = Utils.setDisplayName(new ItemStack(Material.MAGENTA_GLAZED_TERRACOTTA, 1), ChatColor.RED + "Return to Main Menu", "mainmenu");

        ItemStack previous = Utils.setDisplayName(new ItemStack(Material.ARROW, 1), ChatColor.AQUA + "Previous Page", "recentTransactions");
        ItemMeta previousMeta = previous.getItemMeta();
        previousMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "page"), PersistentDataType.INTEGER, (page == 1 ? page : page - 1));
        previous.setItemMeta(previousMeta);

        ItemStack next = Utils.setDisplayName(new ItemStack(Material.ARROW, 1), ChatColor.AQUA + "Next Page", "recentTransactions");
        ItemMeta nextMeta = next.getItemMeta();
        nextMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "page"), PersistentDataType.INTEGER, page + 1);
        next.setItemMeta(nextMeta);

        ItemStack currentPage = Utils.setDisplayName(new ItemStack(Material.OAK_SIGN, 1), ChatColor.AQUA + "Page " + page, "filler");

        ItemStack markAsRead = Utils.setDisplayName(new ItemStack(Material.BOOK, 1), ChatColor.RED + "Mark all as read", "readall");
        ItemMeta markAsReadMeta = markAsRead.getItemMeta();
        markAsReadMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "page"), PersistentDataType.INTEGER, (page == 1 ? page : page - 1));
        markAsRead.setItemMeta(markAsReadMeta);

        inv.setItem(4, mainMenu);
        inv.setItem(40, markAsRead);
        inv.setItem(45, previous);
        inv.setItem(49, currentPage);
        inv.setItem(53, next);

        Utils.getTransactions(player.getUniqueId().toString(), page).thenAccept(transactions -> {
            int invPos = 10;

           for (Transaction transaction : transactions) {
               OfflinePlayer curPlayer = Bukkit.getOfflinePlayer(UUID.fromString(transaction.getUuid()));

               String action = "Purchased";
               if (transaction.getAction().equals("SELL")) action = "Sold";

               ItemStack curItem = new ItemStack(Material.CHEST, 1);
               ItemMeta curMeta = curItem.getItemMeta();
               curMeta.setDisplayName(ChatColor.GOLD + "[" + ChatColor.GREEN + "Transaction Information" + ChatColor.GOLD + "]");

               List<String> lore = new ArrayList<>();
               lore.add(ChatColor.GOLD + "[" + ChatColor.RED + "Player" + ChatColor.GOLD + "] " + ChatColor.GRAY + curPlayer.getName());
               lore.add(ChatColor.GOLD + "[" + ChatColor.AQUA + "Amount" + ChatColor.GOLD + "] " + ChatColor.GRAY + transaction.getAmount());
               lore.add(ChatColor.GOLD + "[" + ChatColor.GREEN + "Price" + ChatColor.GOLD + "] " + ChatColor.GRAY + "$" + transaction.getShopPrice());
               lore.add(ChatColor.GOLD + "[" + ChatColor.YELLOW + action + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.materialName(transaction.getItem().getType()));
               lore.add(ChatColor.GRAY + new Timestamp(transaction.getDate()).toString());
               lore.add(ChatColor.GOLD + "----------------------");
               lore.add("" + ChatColor.GOLD + ChatColor.ITALIC + "Total: " + ChatColor.GRAY + "$" + String.format("%.2f", transaction.getTotal()));

               if (transaction.isUnread()) {
                   lore.add(ChatColor.GOLD + "[" + ChatColor.RED + "Unread" + ChatColor.GOLD + "]");
               }

               curMeta.setLore(lore);
               curMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "transactionId"), PersistentDataType.STRING, transaction.getId());
               curItem.setItemMeta(curMeta);

               inv.setItem(invPos, curItem);
               if (invPos == 16 || invPos == 25) invPos += 2;
               invPos++;
           }

           player.openInventory(inv);
        });
    }
}
