package com.gmail.jd4656.customshops.ui;

import com.gmail.jd4656.customshops.CustomShops;
import com.gmail.jd4656.customshops.Utils;
import com.gmail.jd4656.customshops.types.Shop;
import org.bukkit.*;
import org.bukkit.block.Container;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TransactionDetails {
    Inventory inv;

    TransactionDetails(Player player, String transactionId) {
        inv = Bukkit.createInventory(null, 27, ChatColor.GOLD + "[" + ChatColor.GREEN + "Transaction Information" + ChatColor.GOLD + "]");

        ItemStack fillerItem = Utils.setDisplayName(new ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1), " ", "filler");
        for (int i=0; i<27; i++) inv.setItem(i, fillerItem);

        ItemStack goBack = Utils.setDisplayName(new ItemStack(Material.MAGENTA_GLAZED_TERRACOTTA, 1), ChatColor.RED + "Return to Recent Transactions", "recentTransactions");

        inv.setItem(4, goBack);
        // 12

        Utils.getTransaction(transactionId).thenAccept(transaction -> {
            transaction.markRead();
            OfflinePlayer curPlayer = Bukkit.getOfflinePlayer(UUID.fromString(transaction.getUuid()));
            String action = "Purchased";
            if (transaction.getAction().equals("SELL")) action = "Sold";

            ItemStack curItem = new ItemStack(Material.OAK_SIGN, 1);
            ItemMeta curMeta = curItem.getItemMeta();
            curMeta.setDisplayName(ChatColor.GOLD + "[" + ChatColor.GREEN + "Transaction Information" + ChatColor.GOLD + "]");

            curMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "filler"), PersistentDataType.INTEGER, 1);

            List<String> lore = new ArrayList<>();
            lore.add(ChatColor.GOLD + "[" + ChatColor.RED + "Player" + ChatColor.GOLD + "] " + ChatColor.GRAY + curPlayer.getName());
            lore.add(ChatColor.GOLD + "[" + ChatColor.AQUA + "Amount" + ChatColor.GOLD + "] " + ChatColor.GRAY + transaction.getAmount());
            lore.add(ChatColor.GOLD + "[" + ChatColor.GREEN + "Price" + ChatColor.GOLD + "] " + ChatColor.GRAY + "$" + transaction.getShopPrice());
            lore.add(ChatColor.GOLD + "[" + ChatColor.GREEN + action + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.materialName(transaction.getItem().getType()));
            lore.add(ChatColor.GOLD + "----------------------");
            lore.add("" + ChatColor.GOLD + ChatColor.ITALIC + "Total: " + ChatColor.GRAY + "$" + String.format("%.2f", transaction.getTotal()));

            curMeta.setLore(lore);
            curItem.setItemMeta(curMeta);

            World world = Bukkit.getWorld(transaction.getWorldName());
            Location loc = new Location(world, transaction.getX(), transaction.getY(), transaction.getZ());

            Shop shop = CustomShops.plugin.getShop(loc);

            ItemStack shopInfo = new ItemStack(Material.CHEST, 1);
            ItemMeta shopMeta = shopInfo.getItemMeta();

            PersistentDataContainer shopContainer = shopMeta.getPersistentDataContainer();
            shopContainer.set(new NamespacedKey(CustomShops.plugin, "shopDetails"), PersistentDataType.INTEGER, 1);
            shopContainer.set(new NamespacedKey(CustomShops.plugin, "world"), PersistentDataType.STRING, loc.getWorld().getName());
            shopContainer.set(new NamespacedKey(CustomShops.plugin, "x"), PersistentDataType.INTEGER, loc.getBlockX());
            shopContainer.set(new NamespacedKey(CustomShops.plugin, "y"), PersistentDataType.INTEGER, loc.getBlockY());
            shopContainer.set(new NamespacedKey(CustomShops.plugin, "z"), PersistentDataType.INTEGER, loc.getBlockZ());
            shopMeta.setDisplayName(ChatColor.GOLD + "[" + ChatColor.AQUA + "Shop Information" + ChatColor.GOLD + "]");
            lore = new ArrayList<>();

            if (shop == null) {
                lore.add(ChatColor.GOLD + "[" + ChatColor.RED + "This shop no longer exists" + ChatColor.GOLD + "]");
            } else {
                Container container = (Container) loc.getBlock().getState();
                if (shop.getType().equals("buy")) {
                    lore.add(ChatColor.GOLD + "[" + ChatColor.YELLOW + "Remaining Space" + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.freeSpace(container.getInventory(), transaction.getItem()));
                } else {
                    lore.add(ChatColor.GOLD + "[" + ChatColor.YELLOW + "Stock" + ChatColor.GOLD + "] " + ChatColor.GRAY + Utils.countItems(container.getInventory(), transaction.getItem()));
                }
            }

            lore.add(ChatColor.GOLD + "[" + ChatColor.GREEN + "Location" + ChatColor.GOLD + "] " + ChatColor.GRAY + "X " + transaction.getX() + " Y " + transaction.getY() + " Z " + transaction.getZ());
            shopMeta.setLore(lore);
            shopInfo.setItemMeta(shopMeta);

            ItemStack shopItem = transaction.getItem();
            ItemMeta shopItemMeta = shopItem.getItemMeta();
            shopItemMeta.getPersistentDataContainer().set(new NamespacedKey(CustomShops.plugin, "filler"), PersistentDataType.INTEGER, 1);
            shopItem.setItemMeta(shopItemMeta);

            inv.setItem(12, shopItem);
            inv.setItem(13, curItem);
            inv.setItem(14, shopInfo);

            player.openInventory(inv);
        });
    }
}
