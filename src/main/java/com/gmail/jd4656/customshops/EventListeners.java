package com.gmail.jd4656.customshops;

import com.bekvon.bukkit.residence.containers.Flags;
import com.bekvon.bukkit.residence.protection.ClaimedResidence;
import com.gmail.jd4656.customshops.types.Shop;
import com.gmail.jd4656.customshops.types.ShopAction;
import com.gmail.jd4656.customshops.types.ShopActions;
import com.griefcraft.lwc.LWC;
import com.griefcraft.model.Protection;
import com.palmergames.bukkit.towny.TownyAPI;
import com.palmergames.bukkit.towny.object.TownyPermission;
import com.palmergames.bukkit.towny.utils.PlayerCacheUtil;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Container;
import org.bukkit.block.DoubleChest;
import org.bukkit.block.data.type.WallSign;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class EventListeners implements Listener {
    private CustomShops plugin;
    
    EventListeners(CustomShops p) {
        plugin = p;
    }
    
    @EventHandler 
    public void moveEvent(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (event.getTo() == null) {
            plugin.getLogger().info("getTo null");
            return;
        }
        ShopAction action = plugin.pendingActions.get(player.getUniqueId());
        if (action != null && (!action.getPlayerLocation().getWorld().equals(event.getTo().getWorld()) || action.getPlayerLocation().distance(event.getTo()) > 5)) {
            plugin.pendingActions.remove(player.getUniqueId());
            player.sendMessage(ChatColor.RED + "Cancelled pending shop action");
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        plugin.pendingActions.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        plugin.cacheUUID(player.getUniqueId(), player.getName());

        // TODO rework sale notifications
        plugin.getUnreadLogs(player.getUniqueId()).thenAccept(unread -> {
            if (unread > 0) {
                player.sendMessage(ChatColor.DARK_PURPLE + "You have unread shop notifications, do " + ChatColor.LIGHT_PURPLE + "/shop history " + ChatColor.DARK_PURPLE + "to view them.");
            }
        });
    }

    @EventHandler (priority = EventPriority.LOWEST)
    public void onMessage(AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();
        final String message = event.getMessage();

        ShopAction action = plugin.pendingActions.get(player.getUniqueId());

        if (action != null && (action.getAction() == ShopActions.TRANSACTION || action.getAction() == ShopActions.CREATE)) {
            try {
                Double.parseDouble(message);
                event.setCancelled(true);

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        ShopAction finalAction = plugin.pendingActions.get(player.getUniqueId());
                        if (finalAction == null) return;

                        if (finalAction.getAction() == ShopActions.TRANSACTION) player.chat("/shop amount " + message);
                        if (finalAction.getAction() == ShopActions.CREATE) player.chat("/shop shopprice " + message);
                    }
                }.runTask(plugin);
            } catch (NumberFormatException ignored) {}
        }
    }

    @EventHandler (priority=EventPriority.LOWEST)
    public void blockBreakEvent(BlockBreakEvent event) {
        Block block = event.getBlock();
        Location loc = block.getLocation();
        Player player = event.getPlayer();
        if (!Utils.validShopMaterial(block.getType())) return;

        if (plugin.shops.containsKey(loc)) {
            if (player.getGameMode() == GameMode.CREATIVE) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + "Cancelled block break while in creative to protect shop.");
                return;
            }
            plugin.shops.remove(loc);
            event.getPlayer().sendMessage(ChatColor.RED + "That shop has been removed.");

            new BukkitRunnable() {
                @Override
                public void run() {
                    try {
                        Connection c = plugin.getConnection();
                        PreparedStatement pstmt = c.prepareStatement("DELETE FROM shops WHERE world=? AND x=? AND y=? AND z=?");
                        pstmt.setString(1, loc.getWorld().getName());
                        pstmt.setInt(2, loc.getBlockX());
                        pstmt.setInt(3, loc.getBlockY());
                        pstmt.setInt(4, loc.getBlockZ());
                        pstmt.executeUpdate();
                        pstmt.close();
                    } catch (Exception e) {
                        plugin.getLogger().severe("BlockBreakEvent: " + e.getClass().getName() + ": " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            }.runTaskAsynchronously(plugin);
        }
    }

    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();

        if (!event.getAction().equals(Action.LEFT_CLICK_BLOCK) || block == null) return;
        if ((!Utils.validShopMaterial(block.getType())) && !plugin.signMaterials.contains(block.getType())) return;
        Location loc = block.getLocation();

        if (plugin.signMaterials.contains(block.getType())) {
            WallSign sign = (WallSign) block.getBlockData();
            Block attachedBlock = block.getRelative(sign.getFacing().getOppositeFace());
            if (Utils.validShopMaterial(attachedBlock.getType())) {
                loc = attachedBlock.getLocation();
                block = attachedBlock;
            } else {
                return;
            }
        }

        Container container = (Container) block.getState();
        ShopAction action = plugin.pendingActions.get(player.getUniqueId());
        Shop clickedShop = plugin.shops.get(loc);

        if (clickedShop == null && container.getInventory().getHolder() instanceof DoubleChest) {
            DoubleChest doubleChest = (DoubleChest) container.getInventory().getHolder();
            Chest leftSide = (Chest) doubleChest.getLeftSide();
            Chest rightSide = (Chest) doubleChest.getRightSide();


            if (plugin.shops.containsKey(leftSide.getLocation())) {
                loc = leftSide.getLocation();
                clickedShop = plugin.shops.get(leftSide.getLocation());
                container = (Container) leftSide.getLocation().getBlock().getState();
            }

            if (plugin.shops.containsKey(rightSide.getLocation())) {
                loc = rightSide.getLocation();
                clickedShop = plugin.shops.get(rightSide.getLocation());
                container = (Container) rightSide.getLocation().getBlock().getState();
            }
        }

        if (action != null) {
            if (action.getAction() == ShopActions.FIX) {
                plugin.pendingActions.remove(player.getUniqueId());
                Inventory containerInv = container.getInventory();

                for (ItemStack curItem : containerInv.getContents()) {
                    if (curItem == null) continue;
                    containerInv.removeItem(curItem);
                    containerInv.addItem(plugin.fixItem(curItem));
                }

                player.sendMessage(ChatColor.GREEN + "This shop has been fixed.");
                return;
            }

            if (action.getAction() == ShopActions.CHANGE_TYPE) {
                if (clickedShop == null) return;

                if (!clickedShop.canEdit(player)) return;

                clickedShop.setType(action.getActionArg());
                clickedShop.save();

                plugin.pendingActions.remove(player.getUniqueId());
                player.sendMessage(ChatColor.GREEN + "This shop now " + (action.getActionArg().equals("buy") ? "buys" : "sells") + " items.");

                event.setCancelled(true);
                return;
            }

            if (action.getAction() == ShopActions.CHANGE_OWNER) {
                if (clickedShop == null) return;

                clickedShop.setOwner(action.getActionArg());
                clickedShop.save();
                plugin.pendingActions.remove(player.getUniqueId());

                player.sendMessage(ChatColor.GREEN + "The owner on that shop has been changed.");

                event.setCancelled(true);
                return;
            }

            if (action.getAction() == ShopActions.SET_LIMIT) {
                if (clickedShop == null) return;

                if (!clickedShop.canEdit(player)) return;

                int amount = 0;
                try {
                    amount = Integer.parseInt(action.getActionArg());
                } catch (NumberFormatException ignored) {
                }

                plugin.pendingActions.remove(player.getUniqueId());
                plugin.updateShopLimits(clickedShop.getGroupId(), clickedShop.getOwner(), amount, player);
                player.sendMessage(ChatColor.GREEN + "You've set the limit on that shop to " + ChatColor.GOLD + amount);

                event.setCancelled(true);
                return;
            }

            if (action.getAction() == ShopActions.SET_MODIFIER) {
                if (clickedShop == null) return;
                if (!clickedShop.canEdit(player)) return;

                double percentage = 0;
                int amount = 0;
                try {
                    percentage = Double.parseDouble(action.getActionArgs()[0]);
                    amount = Integer.parseInt(action.getActionArgs()[1]);
                } catch (NumberFormatException ignored) {
                }

                final double finalPercentage = percentage;
                final int finalAmount = amount;
                String owner = clickedShop.getOwner();
                String groupId = clickedShop.getGroupId();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            Connection c = plugin.getConnection();
                            PreparedStatement pstmt = c.prepareStatement("UPDATE groups SET priceModifier=?, priceModifierAmount=? WHERE owner=? AND groupId=?");

                            pstmt.setDouble(1, finalPercentage);
                            pstmt.setInt(2, finalAmount);
                            pstmt.setString(3, owner);
                            pstmt.setString(4, groupId);

                            pstmt.executeUpdate();
                            pstmt.close();
                        } catch (Exception e) {
                            plugin.getLogger().warning("Error setting price modifier:");
                            e.printStackTrace();
                        }
                    }
                }.runTaskAsynchronously(plugin);

                plugin.pendingActions.remove(player.getUniqueId());
                player.sendMessage(ChatColor.GREEN + "You've set the price modifier on that shop to " + percentage + "% for every " + amount + " items.");

                event.setCancelled(true);
                return;
            }

            if (action.getAction() == ShopActions.RESET_TIME) {
                if (clickedShop == null) return;
                if (!clickedShop.canEdit(player)) return;

                int time = plugin.stringToTime(action.getActionArg());
                String owner = clickedShop.getOwner();
                String groupId = clickedShop.getGroupId();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            Connection c = plugin.getConnection();
                            PreparedStatement pstmt = c.prepareStatement("UPDATE groups SET resetTime=?, resetsAt=? WHERE owner=? AND groupId=?");
                            pstmt.setInt(1, time);
                            pstmt.setLong(2, System.currentTimeMillis() + time);
                            pstmt.setString(3, owner);
                            pstmt.setString(4, groupId);

                            pstmt.executeUpdate();
                            pstmt.close();
                        } catch (Exception e) {
                            plugin.getLogger().warning("Error setting reset time on shop:");
                            e.printStackTrace();
                        }
                    }
                }.runTaskAsynchronously(plugin);

                if (time != 0) {
                    String[] id = {groupId, owner};
                    plugin.resetTimes.put(id, System.currentTimeMillis() + time);
                }


                plugin.pendingActions.remove(player.getUniqueId());
                player.sendMessage(ChatColor.GREEN + "You've set the reset time on that shop to " + plugin.convertTime(time));

                event.setCancelled(true);
                return;
            }

            if (action.getAction() == ShopActions.SET_GROUP) {
                if (clickedShop == null) return;
                if (!clickedShop.canEdit(player)) return;

                String owner = clickedShop.getOwner();
                String groupId = clickedShop.getGroupId();
                String newGroupId = action.getActionArg();

                clickedShop.setGroupId(newGroupId);
                clickedShop.save();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            PreparedStatement pstmt;
                            ResultSet rs;
                            Connection c = plugin.getConnection();

                            pstmt = c.prepareStatement("SELECT count(*) FROM shops WHERE owner=? AND groupId=?");
                            pstmt.setString(1, owner);
                            pstmt.setString(2, groupId);
                            rs = pstmt.executeQuery();

                            if (rs.getInt("count(*)") > 1) {
                                pstmt = c.prepareStatement("INSERT INTO groups (owner, groupId) VALUES (?, ?)");
                                pstmt.setString(1, owner);
                                pstmt.setString(2, newGroupId);
                                pstmt.executeUpdate();
                            } else {
                                pstmt = c.prepareStatement("UPDATE groups SET groupId=? WHERE owner=? AND groupId=?");
                                pstmt.setString(1, newGroupId);
                                pstmt.setString(2, owner);
                                pstmt.setString(3, groupId);
                                pstmt.executeUpdate();
                            }

                            pstmt = c.prepareStatement("SELECT count(*) FROM shops WHERE groupId=? AND owner=?");
                            pstmt.setString(1, groupId);
                            pstmt.setString(2, owner);
                            rs = pstmt.executeQuery();

                            if (rs.getInt("count(*)") < 1) {
                                pstmt = c.prepareStatement("DELETE FROM groups WHERE groupId=? AND owner=?");
                                pstmt.setString(1, groupId);
                                pstmt.setString(2, owner);
                                pstmt.executeUpdate();
                            }

                            pstmt.close();
                        } catch (Exception e) {
                            plugin.getLogger().info("shop setGroupId:");
                            e.printStackTrace();
                        }
                    }
                }.runTaskAsynchronously(plugin);

                plugin.pendingActions.remove(player.getUniqueId());
                player.sendMessage(ChatColor.GOLD + "You've set the group on that shop to " + newGroupId);
                return;
            }

            if (action.getAction() == ShopActions.RESET_MODIFIERS) {
                if (clickedShop == null) return;
                if (!clickedShop.canEdit(player)) return;

                String groupId = clickedShop.getGroupId();
                String owner = clickedShop.getOwner();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            Connection c = plugin.getConnection();
                            PreparedStatement pstmt = c.prepareStatement("DELETE FROM users WHERE groupId=? AND owner=?");
                            pstmt.setString(1, groupId);
                            pstmt.setString(2, owner);
                            pstmt.executeUpdate();
                            pstmt.close();
                        } catch (Exception e) {
                            plugin.getLogger().info("shop resetModifiers:");
                            e.printStackTrace();
                        }
                    }
                }.runTaskAsynchronously(plugin);

                plugin.pendingActions.remove(player.getUniqueId());
                player.sendMessage(ChatColor.GOLD + "The price modifier and buy/sell limit on that shop has been reset.");
                return;
            }

            if (action.getAction() == ShopActions.CHANGE_PRICE) {
                if (clickedShop == null) return;
                if (!clickedShop.canEdit(player)) return;

                double newPrice = 0;
                try {
                    newPrice = Double.parseDouble(action.getActionArg());
                } catch (NumberFormatException ignored) {
                }

                clickedShop.setPrice(newPrice);
                clickedShop.updateSign();
                clickedShop.save();

                plugin.pendingActions.remove(player.getUniqueId());
                player.sendMessage(ChatColor.GOLD + "You've set the price on that shop to " + String.format("%.2f", newPrice));
                return;
            }

            if (action.getAction() == ShopActions.SET_PERMISSION) {
                if (clickedShop == null) return;
                if (!clickedShop.canEdit(player)) return;

                clickedShop.setPermission(action.getActionArg());
                clickedShop.save();
                plugin.pendingActions.remove(player.getUniqueId());

                if (action.getActionArg().equals("")) {
                    player.sendMessage(ChatColor.GOLD + "You've removed the permission on that shop.");
                } else {
                    player.sendMessage(ChatColor.GOLD + "You've set the permission on that shop to " + ChatColor.RED + "customshops.use." + action.getActionArg());
                }
                return;
            }
        }

        ItemStack heldItem = player.getInventory().getItemInMainHand();
        if (clickedShop == null && heldItem.getType() != Material.AIR && player.hasPermission("customshops.create")) {

            if (plugin.lwc != null) {
                LWC lwc = plugin.lwc.getLWC();
                Protection protection = lwc.findProtection(block);
                if (protection != null && !lwc.canAccessProtection(player, protection)) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to create a shop here.");
                    return;
                }
            }

            if (plugin.residence != null) {
                ClaimedResidence res = plugin.residence.getResidenceManager().getByLoc(loc);
                if (res != null && (!res.getOwnerUUID().equals(player.getUniqueId()) && !res.getPermissions().playerHas(player, Flags.container, false))) {
                    player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to create a shop here.");
                    return;
                }
            }

            if (TownyAPI.getInstance() != null && !PlayerCacheUtil.getCachePermission(player, loc, block.getType(), TownyPermission.ActionType.SWITCH)) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to create a shop here.");
                return;
            }

            action = new ShopAction();
            action.setAction(ShopActions.CREATE);
            action.setPlayerLocation(player.getLocation());
            action.setShopLocation(loc);
            action.setItem(heldItem);

            plugin.pendingActions.put(player.getUniqueId(), action);
            player.sendMessage(ChatColor.GREEN + "Enter how much you wish to " + ChatColor.AQUA + "SELL" + ChatColor.GREEN + " one " + ChatColor.YELLOW + Utils.materialName(heldItem.getType()) + ChatColor.GREEN + " for in chat.");
            return;
        }

        if (clickedShop != null) {
            clickedShop.updateSign();
            String groupId = clickedShop.getGroupId();
            String type = clickedShop.getType();
            String owner = clickedShop.getOwner();
            final double finalPrice = clickedShop.getPrice();
            ItemStack item = clickedShop.getItem();

            final Container finalContainer = container;
            final Location finalLoc = loc;

            if (!clickedShop.getPermission().equals("") && !player.hasPermission("customshops.use." + clickedShop.getPermission())) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this shop.");
                return;
            }

            plugin.getGroupInfo(groupId, owner, player).thenAccept(info -> {
                if (info == null) return;

                ShopAction newAction = new ShopAction();
                newAction.setAction(ShopActions.TRANSACTION);
                newAction.setShopLocation(finalLoc);
                newAction.setPlayerLocation(player.getLocation());

                plugin.pendingActions.put(player.getUniqueId(), newAction);

                double price = finalPrice;
                double priceModifier = info.getPriceModifier();
                int priceModifierAmount = info.getPriceModifierAmount();
                int curAmount = info.getCurAmount();
                int limit = info.getLimit();
                long resetsAt = info.getResetsAt();

                if (priceModifier > 0 && curAmount > priceModifierAmount) {
                    double timesToDecrease = Math.floor(curAmount / priceModifierAmount);
                    for (int i = 0; i < timesToDecrease; i++) {
                        if (type.equals("sell")) price = (price + (priceModifier / 100) * price);
                        if (type.equals("buy")) price = (price - (priceModifier / 100) * price);
                    }
                } else if (priceModifier > 0 && curAmount == priceModifierAmount) {
                    if (type.equals("sell")) price = (price + (priceModifier / 100) * price);
                    if (type.equals("buy")) price = (price - (priceModifier / 100) * price);
                }

                OfflinePlayer offlinePlayer = null;
                if (!owner.equals("admin")) offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(owner));

                DecimalFormat df = new DecimalFormat("#.##");
                price = Double.parseDouble(df.format(price));

                TextComponent itemComponent = new TextComponent("Item: ");
                itemComponent.setColor(ChatColor.GREEN.asBungee());

                TextComponent itemName = new TextComponent(Utils.materialName(item.getType()));
                itemName.setColor(ChatColor.YELLOW.asBungee());
                itemName.setHoverEvent(plugin.getItemHover(CraftItemStack.asNMSCopy(item)));

                itemComponent.addExtra(itemName);

                player.sendMessage(ChatColor.GREEN + "Shop Information:");
                player.sendMessage(ChatColor.GREEN + "Owner: " + (offlinePlayer == null ? ChatColor.DARK_RED + "AdminShop" : ChatColor.YELLOW + offlinePlayer.getName()));
                player.spigot().sendMessage(itemComponent);
                if (offlinePlayer != null) player.sendMessage(ChatColor.GREEN + "Stock: " + Utils.countItems(finalContainer.getInventory(), item));
                if (limit > 0) player.sendMessage(ChatColor.GREEN + (!type.equals("buy") ? "Sell" : "Buy") + " limit: " + ChatColor.YELLOW + curAmount + " / " + limit);
                if (resetsAt > 0) player.sendMessage(ChatColor.GREEN + "Resets at: " + ChatColor.YELLOW + plugin.convertTime(resetsAt - System.currentTimeMillis()));
                player.sendMessage(ChatColor.GREEN + "Price: " + ChatColor.YELLOW + String.format("%.2f", price));
                if (priceModifier > 0) player.sendMessage(ChatColor.GREEN + "Price " + (!type.equals("buy") ? "increases" : "decreases") + " by " + ChatColor.YELLOW + priceModifier + "%" + ChatColor.GREEN + " every " + ChatColor.YELLOW + priceModifierAmount + ChatColor.GREEN + " items.");

                // TODO clean up this mess
                if (item.hasItemMeta()) {
                    ItemMeta meta = item.getItemMeta();
                    if (meta.hasEnchants()) {
                        String output = "";
                        Map enchants = meta.getEnchants();
                        Iterator it = enchants.entrySet().iterator();
                        while (it.hasNext()) {
                            if (!output.equals("")) output += ", ";
                            Map.Entry<Enchantment, Integer> pair = (Map.Entry) it.next();
                            Enchantment enchantment = pair.getKey();
                            int enchantLevel = pair.getValue();
                            output += enchantment.getName() + " " + enchantLevel;
                        }
                        player.sendMessage(ChatColor.GREEN + "Enchantments: " + ChatColor.YELLOW + output);
                    }
                    if (item.getType().equals(Material.ENCHANTED_BOOK)) {
                        EnchantmentStorageMeta bookMeta = (EnchantmentStorageMeta) item.getItemMeta();
                        if (bookMeta.hasStoredEnchants()) {
                            String output = "";
                            Map enchants = bookMeta.getStoredEnchants();
                            Iterator it = enchants.entrySet().iterator();
                            while (it.hasNext()) {
                                if (!output.equals("")) output += ", ";
                                Map.Entry<Enchantment, Integer> pair = (Map.Entry) it.next();
                                Enchantment enchantment = pair.getKey();
                                int enchantLevel = pair.getValue();
                                output += enchantment.getName() + " " + enchantLevel;
                            }
                            player.sendMessage(ChatColor.GREEN + "Enchantments: " + ChatColor.YELLOW + output);
                        }
                    }
                    if (item.getType().equals(Material.POTION) || item.getType().equals(Material.LINGERING_POTION) || item.getType().equals(Material.SPLASH_POTION)) {
                        PotionMeta potionMeta = (PotionMeta) item.getItemMeta();
                        String output = "";
                        PotionData potionData = potionMeta.getBasePotionData();
                        output += potionData.getType().name();
                        if (potionMeta.hasCustomEffects()) {
                            potionMeta.getCustomEffects();
                            List<PotionEffect> effects = potionMeta.getCustomEffects();
                            for (PotionEffect effect : effects) {
                                if (!output.equals("")) output += ", ";
                                int seconds = (effect.getDuration() / 20) % 60;
                                int minutes = (seconds / 60) % 60;
                                output += effect.toString() + "(" + minutes + ":" + seconds + ")";
                            }
                        }
                        player.sendMessage(ChatColor.GREEN + "Effects: " + ChatColor.YELLOW + output);
                    }
                }

                if (type.equals("buy")) {
                    int itemCount = Utils.countItems(player.getInventory(), item);
                    player.sendMessage(ChatColor.GREEN + "You have " + ChatColor.YELLOW + itemCount + ChatColor.GREEN + (itemCount == 1 ? " item" : " items") + " available to sell.");
                }
                player.sendMessage(ChatColor.DARK_GREEN + "Please enter the amount you would like to " + (type.equals("buy") ? ChatColor.LIGHT_PURPLE + "SELL" : ChatColor.AQUA + "BUY") + ": ");
            });
        }
    }
}
